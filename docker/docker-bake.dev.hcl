variable "DOCKER_IMAGE_BASE" {
    default = "gitlab.com/eemj/image-processor"
}

variable "TAG" {
    default = "devel"
}

variable "GOLANG_VERSION" {
    default = "1.23"
}

group "default" {
    targets = ["api", "migrate", "consumer"]
}

target "api" {
    output = ["type=registry"]
    dockerfile = "docker/Dockerfile.api"
    tags = ["${DOCKER_IMAGE_BASE}/api:${TAG}"]
    args = {
        GOLANG_VERSION = GOLANG_VERSION
    }
}

target "migrate" {
    output = ["type=registry"]
    dockerfile = "docker/Dockerfile.migrate"
    tags = ["${DOCKER_IMAGE_BASE}/migrate:${TAG}"]
    args = {
        GOLANG_VERSION = GOLANG_VERSION
    }
}

target "consumer" {
    output = ["type=registry"]
    dockerfile = "docker/Dockerfile.consumer"
    tags = ["${DOCKER_IMAGE_BASE}/consumer:${TAG}"]
    args = {
        GOLANG_VERSION = GOLANG_VERSION
    }
}
