package common

const (
	// AnalyzerServiceName returns `analyzer_service`
	AnalyzerServiceName = "analyzer_service"

	// UploadServiceName returns `upload_service`
	UploadServiceName = "upload_service"

	// FilterServiceName returns `filter_service`
	FilterServiceName = "filter_service"

	// ConvertConsumerHandlerName returns `convert_consumer_handler`
	ConvertConsumerHandlerName = "convert_consumer_handler"

	// NATSPublisherName returns `nats_publisher`
	NATSPublisherName = "nats_publisher"

	// NATSConsumerName returns `nats_consumer`
	NATSConsumerName = "nats_consumer"
)
