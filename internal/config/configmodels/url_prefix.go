package configmodels

import (
	"errors"

	stringutil "go.jamie.mt/toolbox/string_util"
)

type URLPrefix struct {
	ContentDelivery string `json:"contentDelivery" mapstructure:"content_delivery"`
}

func (u *URLPrefix) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(u.ContentDelivery) {
		return errors.New("`url_prefix.content_delivery` should not be empty")
	}

	return
}
