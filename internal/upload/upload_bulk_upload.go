package upload

import (
	"context"
	"errors"
	"runtime"
	"slices"
	"sync"

	"github.com/segmentio/ksuid"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BulkUploadImagesParams []UploadImageParams

func (b BulkUploadImagesParams) Validate() (err error) {
	for index, param := range b {
		if validateErr := param.Validate(); validateErr != nil {
			err = errors.Join(
				err,
				status.Errorf(
					codes.InvalidArgument,
					"`%v` for request at index `%d`", validateErr, index,
				),
			)
		}
	}

	return err
}

// MergeUploadImageParams iterates through all the parameters, applies a distinct on
// the URL paths and ensures that the `format` slice is merged, whilst ensuring the
// `computeDominantColor` is the most truthful.
func mergeUploadImageParams(params ...UploadImageParams) []UploadImageParams {
	if len(params) <= 1 {
		return params
	}

	urlParams := make(map[string][]UploadImageParams)
	for _, param := range params {
		_, exists := urlParams[param.URL]
		if exists {
			urlParams[param.URL] = make([]UploadImageParams, 0)
		}
		urlParams[param.URL] = append(urlParams[param.URL], param)
	}

	mergedParams := make([]UploadImageParams, 0, len(urlParams))
	for _, groupedParams := range urlParams {
		var (
			formats    = make([]enums.ImageFormat, 0)
			parameters domain.ImageRequestParameters
		)

		for _, currentParams := range groupedParams {
			for _, format := range currentParams.ImageFormats {
				if !slices.Contains(formats, format) {
					formats = append(formats, format)
				}
			}

			if currentParams.Parameters.ComputeDominantColor && !parameters.ComputeDominantColor {
				parameters = currentParams.Parameters
			}
		}

		mergedParams = append(mergedParams, UploadImageParams{
			URL:          groupedParams[0].URL,
			ImageFormats: formats,
			Parameters:   parameters,
		})
	}

	return mergedParams
}

type BulkUploadImageWork struct{}

type BulkUploadImagesResultItem struct {
	Error     error       `json:"error,omitempty"`
	Reference ksuid.KSUID `json:"reference,omitempty"`
	PublicURL string      `json:"public_url,omitempty"`
}

type BulkUploadImagesResult struct {
	Params map[string]BulkUploadImagesResultItem `json:"params,omitempty"`
}

func (s *service) BulkUpload(ctx context.Context, params BulkUploadImagesParams) (*BulkUploadImagesResult, error) {
	ctx, span := s.tracer.Start(ctx, common.UploadServiceBulkUploadSpan)
	defer span.End()

	params = mergeUploadImageParams(params...)
	err := params.Validate()
	if err != nil {
		return nil, err
	}

	var (
		semaphore = make(chan struct{}, runtime.NumCPU())
		wg        sync.WaitGroup
		mu        sync.Mutex

		result = &BulkUploadImagesResult{
			Params: make(map[string]BulkUploadImagesResultItem, len(params)),
		}
	)

	wg.Add(len(params))

	for _, param := range params {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case semaphore <- struct{}{}:
		}

		go func(param UploadImageParams) {
			defer func() {
				<-semaphore
				wg.Done()
			}()

			s.L(ctx).
				Sugar().
				Infof("processing `%s` upload", param.URL)

			uploadResult, uploadErr := s.Upload(ctx, param)

			mu.Lock() // prevent concurrent writes to the map
			defer mu.Unlock()

			{
				if uploadErr != nil {
					result.Params[param.URL] = BulkUploadImagesResultItem{
						Error: uploadErr,
					}
				} else {
					result.Params[param.URL] = BulkUploadImagesResultItem{
						PublicURL: uploadResult.PublicURL,
						Reference: uploadResult.Reference,
					}
				}
			}
		}(param)
	}

	wg.Wait() // Wait for the uploads to finalize

	return result, nil
}
