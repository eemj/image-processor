package common

const (
	// UploadServiceSettingsSpan returns `UploadService.Settings`
	UploadServiceSettingsSpan = "UploadService.Settings"
	// UploadServiceUploadSpan returns `UploadService.Upload`
	UploadServiceUploadSpan = "UploadService.Upload"
	// UploadServiceBulkUploadSpan returns `UploadService.BulkUpload`
	UploadServiceBulkUploadSpan = "UploadService.BulkUpload"
	// FilterServiceFilterSpan returns `FilterService.Filter`
	FilterServiceFilterSpan = "FilterService.Filter"
)
