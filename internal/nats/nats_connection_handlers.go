package nats

import (
	"context"

	"github.com/nats-io/nats.go"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

func handlerLog(ctx context.Context) *zap.Logger {
	return log.WithTrace(ctx).Named("nats_handler")
}

func (n *Connection) connectHandler(c *nats.Conn) {
	ctx, span := n.tracer.Start(context.TODO(), "NATSConnection - Connect")
	defer span.End()

	handlerLog(ctx).Info(
		"connected",
		zap.Strings("servers", c.Servers()),
	)
}

func (n *Connection) disconnectErrHandler(c *nats.Conn, err error) {
	ctx, span := n.tracer.Start(context.TODO(), "NATSConnection - Disconnect")
	defer span.End()

	if err != nil {
		handlerLog(ctx).Error(
			"disconnected",
			zap.Strings("servers", c.Servers()),
			zap.Error(err),
		)

		span.RecordError(err)
	} else {
		handlerLog(ctx).Info(
			"disconnected",
			zap.Strings("servers", c.Servers()),
		)
	}
}

func (n *Connection) errorHandler(c *nats.Conn, s *nats.Subscription, err error) {
	ctx, span := n.tracer.Start(context.TODO(), "NATSConnection - Error")
	defer span.End()

	handlerLog(ctx).Error(
		"unhandled error",
		zap.Strings("servers", c.Servers()),
		zap.Error(err),
	)

	span.RecordError(err)
}

func (n *Connection) reconnectHandler(c *nats.Conn) {
	ctx, span := n.tracer.Start(context.TODO(), "NATSConnection - Reconnect")
	defer span.End()

	handlerLog(ctx).Info(
		"reconnected",
		zap.Strings("servers", c.Servers()),
	)
}
