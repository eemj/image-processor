package convert

import (
	"fmt"

	"github.com/google/uuid"
	"go.temporal.io/sdk/temporal"
)

type ErrorType string

const (
	ERROR_TYPE_IMAGE_NOT_FOUND  ErrorType = "ImageNotFound"
	ERROR_TYPE_OBJECT_NOT_FOUND ErrorType = "ObjectNotFound"
	ERROR_TYPE_ENTITY_NOT_FOUND ErrorType = "EntityNotFound"
)

func ErrOriginalImageInRequestIDNotFound(imageRequestID uuid.UUID) error {
	return temporal.NewNonRetryableApplicationError(
		"original image not found",
		string(ERROR_TYPE_IMAGE_NOT_FOUND),
		fmt.Errorf("original image in request id '%s' not found", imageRequestID),
	)
}

func ErrImageRequestIDNotFound(imageRequestID uuid.UUID) error {
	return temporal.NewNonRetryableApplicationError(
		"image request not found",
		string(ERROR_TYPE_ENTITY_NOT_FOUND),
		fmt.Errorf("image request with id '%s' not found", imageRequestID),
	)
}

func ErrObjectKeyNotFound(objectKey string) error {
	return temporal.NewNonRetryableApplicationError(
		"object key not found",
		string(ERROR_TYPE_OBJECT_NOT_FOUND),
		fmt.Errorf("object with key '%s' not found", objectKey),
	)
}
