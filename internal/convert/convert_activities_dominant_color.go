package convert

import (
	"context"
	"errors"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"strings"

	_ "github.com/chai2010/webp"
	"go.uber.org/zap"

	"github.com/EdlinOrg/prominentcolor"
	"github.com/davidbyttow/govips/v2/vips"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/minio/minio-go/v7"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain/bitflags"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
)

var imageDecodeFormats = map[enums.ImageFormat]bool{
	enums.IMAGE_FORMAT_JPEG: true,
	enums.IMAGE_FORMAT_WEBP: true,
	enums.IMAGE_FORMAT_PNG:  true,
}

type (
	ProcessDominantColorInput struct {
		ImageRequestID        uuid.UUID `json:"image_request_id,omitempty"`
		FallbackDominantColor *string   `json:"fallback_dominant_color"`
	}
)

// ProcessDominantColour will temporarily return `DominantColour` as nil
// till we refactor the dominant color logic - We're getting memory leaks
// causing the app to crash under high workloads.
func (c *consumerHandler) processDominantColour(ctx context.Context, input *ProcessDominantColorInput) error {
	rows, err := c.db.Q().FindImagesWithImageRequestID(ctx, sqlc.FindImagesWithImageRequestIDParams{
		ImageRequestID: input.ImageRequestID,
	})
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		return err
	}
	if len(rows) == 0 {
		return ErrImageRequestIDNotFound(input.ImageRequestID)
	}

	// Try to re-use if processed the dominant color already for this `ImageRequestID`
	var dominantColor *string
	if len(rows) > 0 {
		dominantColor = rows[0].DominantColor
	}
	if dominantColor != nil {
		return nil
	}

	// Determine the following images:
	//  Smallest: a supported processed image - to avoid converting to something supported
	//  Original: fallback in-case we don't find a supported processed image
	var (
		originalRow *sqlc.FindImagesWithImageRequestIDRow
		smallestRow *sqlc.FindImagesWithImageRequestIDRow
	)
	for _, row := range rows {
		if bitflags.Has(row.Image.Flags, bitflags.IMAGE_FLAG_ORIGINAL) {
			originalRow = row
		}

		if imageDecodeFormats[row.Image.Format] &&
			(smallestRow == nil || smallestRow.Image.Size > row.Image.Size) {
			smallestRow = row
		}
	}

	var objectKey string
	if smallestRow != nil {
		objectKey = smallestRow.Image.ObjectKey
	} else if originalRow != nil {
		objectKey = originalRow.Image.ObjectKey
	} else {
		return ErrOriginalImageInRequestIDNotFound(input.ImageRequestID)
	}

	objectInfo, err := c.minioClient.GetObject(
		ctx,
		c.s3Config.Bucket,
		objectKey,
		minio.GetObjectOptions{Checksum: false},
	)
	if err != nil {
		errorResponse, ok := err.(minio.ErrorResponse)
		if ok && errorResponse.Code == "NoSuchKey" {
			return ErrObjectKeyNotFound(objectKey)
		}
		return err
	}
	defer objectInfo.Close()

	var img image.Image
	if smallestRow != nil {
		img, _, err = image.Decode(objectInfo)
		if err != nil {
			return err
		}
	} else {
		vipsRef, err := vips.NewImageFromReader(objectInfo)
		if err != nil {
			return err
		}
		defer vipsRef.Close()

		// If we have an alpha layer, we need to convert into
		// a format that supports alpha - otherwise interlacing will apply
		// on a JPEG image.
		var exportParams *vips.ExportParams
		if vipsRef.HasAlpha() {
			exportParams = vips.NewDefaultPNGExportParams()
		} else {
			exportParams = vips.NewDefaultJPEGExportParams()
		}

		img, err = vipsRef.ToImage(exportParams)
		if err != nil {
			return err
		}

		// We're practically done with the vips image - unref
		vipsRef.Close()
	}

	colors, err := prominentcolor.Kmeans(img)
	if err != nil || len(colors) == 0 { // Unsure whether this will return empty - but apply the fallback
		if input.FallbackDominantColor != nil && *input.FallbackDominantColor != "" {
			c.logger.Error(
				"failed to compute the `dominant_color`, using fallback",
				zap.Error(err),
			)

			fallbackDominantColor := strings.TrimFunc(
				strings.ToLower(*input.FallbackDominantColor),
				func(r rune) bool { return r == '#' || r == ' ' },
			)
			dominantColor = &fallbackDominantColor
		}
	} else {
		dominantColor = &[]string{colors[0].AsString()}[0]
	}

	if dominantColor != nil {
		err = c.db.Q().UpdateImageRequest(ctx, sqlc.UpdateImageRequestParams{
			ID:            input.ImageRequestID,
			DominantColor: dominantColor,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
