package database

import (
	"context"
	"fmt"
	"strings"

	"github.com/exaring/otelpgx"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"go.jamie.mt/toolbox/configmodels"
)

type DB interface {
	Q() sqlc.Querier
	Pool() *pgxpool.Pool
	Close()
	Ping(ctx context.Context) error
}

// DB is a database wrapper which holds the pgx connection pool and
// a declaration of the `sqlc` generated methods.
type db struct {
	querier sqlc.Querier
	pool    *pgxpool.Pool
}

var dataTypeNames = [...]string{
	"image_format", "_image_format",
}

func getCustomDataTypes(ctx context.Context, pool *pgxpool.Pool) ([]*pgtype.Type, error) {
	// Get a single connection just to load type information.
	conn, err := pool.Acquire(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Release()

	typesToRegister := make([]*pgtype.Type, len(dataTypeNames))
	for index, typeName := range dataTypeNames {
		dataType, err := conn.Conn().LoadType(ctx, typeName)
		if err != nil {
			return nil, fmt.Errorf("failed to load type %s: %v", typeName, err)
		}
		conn.Conn().TypeMap().RegisterType(dataType)
		typesToRegister[index] = dataType
	}
	return typesToRegister, nil
}

func registerCustomTypes(ctx context.Context, config configmodels.PostgreSQL) (*pgxpool.Pool, error) {
	cfg, err := pgxpool.ParseConfig(config.UnsafeString())
	if err != nil {
		return nil, err
	}
	cfg.ConnConfig.Tracer = otelpgx.NewTracer(
		otelpgx.WithIncludeQueryParameters(),
		otelpgx.WithTrimSQLInSpanName(),
		otelpgx.WithSpanNameFunc(func(stmt string) string {
			// Customize the span name based on the new `sqlc` method
			// -- name: ListAnimeRelationsWithMappings :many
			sqlcMethod, found := strings.CutPrefix(stmt, "-- name: ")
			if found {
				whitespaceIndex := strings.IndexRune(sqlcMethod, ' ')
				if whitespaceIndex > -1 {
					return sqlcMethod[:whitespaceIndex]
				}
			}
			// Fallback to the fields function -
			// try to get the SQL operation
			parts := strings.Fields(stmt)
			if len(parts) == 0 {
				return "?"
			}
			return strings.ToUpper(parts[0])
		}),
	)
	pool, err := pgxpool.NewWithConfig(ctx, cfg)
	if err != nil {
		return nil, err
	}

	customDataTypes, err := getCustomDataTypes(ctx, pool)
	if err != nil {
		return nil, err
	}

	// Load all custom types
	cfg.AfterConnect = func(_ context.Context, c *pgx.Conn) error {
		for _, dataType := range customDataTypes {
			c.TypeMap().RegisterType(dataType)
		}
		return nil
	}

	// Close the current pool
	pool.Close()

	// Re-open for the AfterConnect to take action.
	return pgxpool.NewWithConfig(ctx, cfg)
}

func NewDB(config configmodels.PostgreSQL) (DB, error) {
	pool, err := registerCustomTypes(context.Background(), config)
	if err != nil {
		return nil, err
	}
	return &db{
		pool:    pool,
		querier: sqlc.New(pool),
	}, nil
}

// Q returns an instance of `sqlc.Querier` which contains
// all `sqlc` generated methods that interact with the application's domain.
func (d *db) Q() sqlc.Querier {
	return d.querier
}

// Pool returns the `*pgxpool.Pool` created whilst calling the
// `NewDB()` function.
func (d *db) Pool() *pgxpool.Pool {
	return d.pool
}

// Close closes the pgx pool.
func (d *db) Close() {
	d.pool.Close()
	if d.pool != nil {
		d.pool.Close()
	}
}

// Ping pings the database.
func (d *db) Ping(ctx context.Context) error {
	return d.pool.Ping(ctx)
}
