package config

import (
	"errors"

	"gitlab.com/eemj/image-processor/internal/config/configmodels"
	customconfig "go.jamie.mt/toolbox/configmodels"
)

type API struct {
	URLPrefix configmodels.URLPrefix `json:"urlPrefix" mapstructure:"url_prefix"`

	Vips *configmodels.Vips                 `json:"vips" mapstructure:"vips"`
	NATS *configmodels.NATSConnectionConfig `json:"nats" mapstructure:"nats"`

	Redis     customconfig.Redis      `json:"redis" mapstructure:"redis"`
	GRPC      customconfig.GRPCServer `json:"grpc" mapstructure:"grpc"`
	S3        customconfig.S3         `json:"s3" mapstructure:"s3"`
	Database  customconfig.PostgreSQL `json:"database" mapstructure:"database"`
	Logging   customconfig.Logging    `json:"logging" mapstructure:"logging"`
	Telemetry customconfig.Telemetry  `json:"telemetry" mapstructure:"telemetry"`

	Path string `json:"-"`
}

func (c *API) SetPath(path string) { c.Path = path }

func (c *API) Validate() (err error) {
	err = errors.Join(
		c.URLPrefix.Validate(),
		c.Redis.Validate(),
		c.GRPC.Validate(),
		c.S3.Validate(),
		c.Database.Validate(),
		c.Logging.Validate(),
		c.Telemetry.Validate(),
	)
	if c.NATS == nil {
		err = errors.Join(err, errors.New("`nats` is required"))
	} else {
		err = errors.Join(err, c.NATS.Validate())
	}
	if c.Vips == nil { // It's optional - if it's not provided, use defaults
		c.Vips = configmodels.DefaultVips
	} else {
		err = errors.Join(err, c.Vips.Validate())
	}
	return err
}
