package analyzer

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"net/http"

	"github.com/davidbyttow/govips/v2/vips"
	"github.com/jackc/pgx/v5"
	"github.com/segmentio/ksuid"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
	ioutil "gitlab.com/eemj/image-processor/pkg/io_util"
)

type AnalyzerPrepareParams struct {
	URL string
}

type AnalyzerPrepareResult struct {
	Buffer       *bytes.Buffer
	SHA1         string
	ImageRequest *domain.ImageRequest
	Reference    ksuid.KSUID
	ImageFormat  enums.ImageFormat
	ImageSize    *domain.ImageDimensions
}

func (s *service) Prepare(ctx context.Context, param AnalyzerPrepareParams) (*AnalyzerPrepareResult, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, param.URL, nil)
	if err != nil {
		return nil, err
	}
	res, err := s.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode < 200 && res.StatusCode > 299 {
		return nil, fmt.Errorf(
			"unexpected status code '%d' (%s)",
			res.StatusCode,
			http.StatusText(res.StatusCode),
		)
	}

	// Retrieve the image, SHA1 it and check if we have any
	// image related to this SHA1 by checking the `image_request` and
	// all the `images`.
	var bufferBytes []byte
	if res.ContentLength >= 0 {
		bufferBytes = make([]byte, 0, res.ContentLength)
	}
	var (
		hash       = sha1.New()
		buffer     = bytes.NewBuffer(bufferBytes)
		typeBuffer = ioutil.LimitBuffer(12) // `govips` requires 12 bytes to determine

		writer = io.MultiWriter(
			hash,       // SHA1
			buffer,     // Image Bytes
			typeBuffer, // First 12 bytes of the Image (used for filetype determination)
		)
	)

	_, err = io.Copy(writer, res.Body)
	if err != nil {
		return nil, err
	}

	sha1Hex := hex.EncodeToString(hash.Sum(nil))

	// Free
	hash.Reset()
	hash = nil

	// Determine the image (quick 12 byte scan to see if we should proceed)
	vipsImageType := vips.DetermineImageType(typeBuffer.B)
	imageFormat, ok := enums.VipsImageFormats[vipsImageType]
	if !ok {
		return nil, errors.New("unsupported filetype")
	}

	// Free
	typeBuffer.Reset()

	// Given the SHA1 hash, check if there are other images based on this
	rows, err := s.db.Q().FindImagesWithSha1(ctx, sqlc.FindImagesWithSha1Params{
		SHA1: sha1Hex,
	})
	if err != nil && err != pgx.ErrNoRows {
		return nil, err
	}
	imageRequest := transformFindImagesWithSHA1(rows...)

	// If there are images, we'll use the data for the first image
	var (
		imageSize *domain.ImageDimensions
		reference ksuid.KSUID
	)
	if imageRequest != nil && len(imageRequest.Images) > 0 {
		reference = imageRequest.Reference
		imageSize = &domain.ImageDimensions{
			Height: int64(imageRequest.Images[0].Height),
			Width:  int64(imageRequest.Images[0].Width),
		}
	} else { // Otherwise, use `govips` to get the image size and generate a new `reference`
		reference = ksuid.New()

		vipsRef, err := vips.NewImageFromBuffer(buffer.Bytes())
		if err != nil {
			return nil, err
		}
		defer vipsRef.Close()

		vipsMetadata := vipsRef.Metadata()
		if vipsMetadata != nil {
			imageSize = &domain.ImageDimensions{
				Height: int64(vipsMetadata.Height),
				Width:  int64(vipsMetadata.Width),
			}
		}
	}

	return &AnalyzerPrepareResult{
		ImageRequest: imageRequest,
		Reference:    reference,
		Buffer:       buffer,
		ImageFormat:  imageFormat,
		SHA1:         sha1Hex,
		ImageSize:    imageSize,
	}, nil
}
