-- vim: set ft=sql:

-- Types
drop type if exists image_format cascade;

-- Tables
drop table if exists image cascade;
drop table if exists image_request cascade;
