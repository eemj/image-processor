package configmodels

import (
	"errors"

	"github.com/nats-io/nats.go"
	stringutil "go.jamie.mt/toolbox/string_util"
)

type NATSConnectionConfig struct {
	URL string

	// Options
	Name        string
	Compression *bool
	Username    *string
	Password    *string
	Token       *string
}

func (n *NATSConnectionConfig) Options() []nats.Option {
	var options []nats.Option
	if !stringutil.IsTrimmedEmpty(n.Name) {
		options = append(options, nats.Name(n.Name))
	}
	if n.Username != nil && n.Password != nil && !stringutil.IsTrimmedEmpty(*n.Username) && !stringutil.IsTrimmedEmpty(*n.Password) {
		options = append(options, nats.UserInfo(*n.Username, *n.Password))
	}
	if n.Token != nil && !stringutil.IsTrimmedEmpty(*n.Token) {
		options = append(options, nats.Token(*n.Token))
	}
	if n.Compression != nil {
		options = append(options, nats.Compression(*n.Compression))
	}
	return options
}

func (n *NATSConnectionConfig) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(n.Name) {
		err = errors.Join(err, errors.New("`nats.name` is required"))
	}
	if stringutil.IsTrimmedEmpty(n.URL) {
		err = errors.Join(err, errors.New(`"nats.url" is required`))
	}
	return err
}
