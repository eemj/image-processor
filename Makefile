GOPATH:=$(shell go env GOPATH)

.PHONY: dependencies
dependencies:
	@go install github.com/sqlc-dev/sqlc/cmd/sqlc@latest
	@go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	@go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest


.PHONY: proto
proto:
	@buf generate \
		--exclude-path vendor/ \
		--error-format github-actions
	@buf generate \
		--template api/grpc_imageprocessor/v1/grpc_imageprocessor.buf.gen.yaml \
		--path api/grpc_imageprocessor/v1/grpc_imageprocessor.proto \
		--error-format github-actions

.PHONY: sqlc
sqlc:
	@sqlc generate

.PHONY: generate
generate:
	make proto;
	make sqlc;

.PHONY: new-migration
new-migration:
	@migrate create -dir $(CURDIR)/cmd/migrate/migrations -format unixNano -ext sql $(filter-out $@,$(MAKECMDGOALS))
