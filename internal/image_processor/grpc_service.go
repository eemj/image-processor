package imageprocessor

import (
	"context"
	"sort"

	grpc_imageprocessor_v1 "gitlab.com/eemj/image-processor/api/grpc_imageprocessor/v1"
	"gitlab.com/eemj/image-processor/internal/database"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
	"gitlab.com/eemj/image-processor/internal/filter"
	"gitlab.com/eemj/image-processor/internal/upload"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type grpcService struct {
	db        database.DB
	uploadSvc upload.Service
	filterSvc filter.Service

	grpc_imageprocessor_v1.UnimplementedImageProcessorServer
}

// BulkUpload implements grpc_imageprocessor_v1.ImageProcessorServer.
func (g *grpcService) BulkUpload(ctx context.Context, req *grpc_imageprocessor_v1.BulkUploadRequest) (*grpc_imageprocessor_v1.BulkUploadResponse, error) {
	params := make(upload.BulkUploadImagesParams, len(req.Items))
	for index, item := range req.Items {
		var (
			parameters domain.ImageRequestParameters
			formats    []enums.ImageFormat
		)
		if item.Parameters != nil {
			parameters = domain.ImageRequestParameters{
				ComputeDominantColor:  item.Parameters.ComputeDominantColor,
				FallbackDominantColor: item.Parameters.FallbackDominantColor,
			}
		}
		if item.Formats != nil {
			for _, format := range item.Formats {
				formats = append(formats, enums.ApiImageFormats[format])
			}
		}
		params[index] = upload.UploadImageParams{
			URL:          item.Url,
			ImageFormats: formats,
			Parameters:   parameters,
		}
	}
	bulkUploadResult, err := g.uploadSvc.BulkUpload(ctx, params)
	if err != nil {
		return nil, err
	}
	response := &grpc_imageprocessor_v1.BulkUploadResponse{
		Items: make(map[string]*grpc_imageprocessor_v1.BulkUploadResponseItem),
	}
	for url, param := range bulkUploadResult.Params {
		item := &grpc_imageprocessor_v1.BulkUploadResponseItem{}
		if param.Error != nil {
			errorMessage := param.Error.Error()
			item.Error = &errorMessage
		} else {
			item.Reference = param.Reference.String()
			item.PublicUrl = param.PublicURL
		}

		response.Items[url] = item
	}
	return response, nil
}

// Filter implements grpc_imageprocessor_v1.ImageProcessorServer.
func (g *grpcService) Filter(ctx context.Context, req *grpc_imageprocessor_v1.FilterRequest) (*grpc_imageprocessor_v1.FilterResponse, error) {
	filterResult, err := g.filterSvc.Filter(ctx, filter.FilterParams{
		References: req.References,
		URLs:       req.Urls,
	})
	if err != nil {
		return nil, err
	}
	response := &grpc_imageprocessor_v1.FilterResponse{
		Items: make([]*grpc_imageprocessor_v1.ImageItem, len(filterResult.Items)),
	}
	for index, item := range filterResult.Items {
		newItem := &grpc_imageprocessor_v1.ImageItem{
			Reference: item.Reference.String(),
			Color:     item.DominantColor,
			OriginUrl: item.OriginURL,
			PublicUrl: item.PublicURL,
			CreatedAt: timestamppb.New(item.CreatedAt),
			Metadata:  make([]*grpc_imageprocessor_v1.ImageMetadata, 0, len(item.Metadata)),
		}
		for _, metadata := range item.Metadata {
			newItem.Metadata = append(newItem.Metadata, &grpc_imageprocessor_v1.ImageMetadata{
				Size:   metadata.Size,
				Format: *metadata.Format.Api().Enum(),
				Checksum: &grpc_imageprocessor_v1.ImageChecksum{
					Sha1: metadata.Checksums.SHA1,
				},
				Dimension: &grpc_imageprocessor_v1.ImageDimension{
					Height: uint64(item.Dimensions.Height),
					Width:  uint64(item.Dimensions.Width),
				},
			})
		}

		// sort by size ascending
		sort.SliceStable(newItem.Metadata, func(i, j int) bool {
			return newItem.Metadata[i].Size < newItem.Metadata[j].Size
		})

		response.Items[index] = newItem
	}
	return response, nil
}

// Settings implements grpc_imageprocessor_v1.ImageProcessorServer.
func (g *grpcService) Settings(ctx context.Context, _ *grpc_imageprocessor_v1.SettingsRequest) (*grpc_imageprocessor_v1.SettingsResponse, error) {
	settingsResult, err := g.uploadSvc.Settings(ctx)
	if err != nil {
		return nil, err
	}
	return &grpc_imageprocessor_v1.SettingsResponse{
		ContentDeliveryBaseUrl: settingsResult.ContentDeliveryBaseURL,
	}, nil
}

// Upload implements grpc_imageprocessor_v1.ImageProcessorServer.
func (g *grpcService) Upload(ctx context.Context, req *grpc_imageprocessor_v1.UploadRequest) (*grpc_imageprocessor_v1.UploadResponse, error) {
	var (
		parameters domain.ImageRequestParameters
		formats    []enums.ImageFormat
	)
	if req.Parameters != nil {
		parameters = domain.ImageRequestParameters{
			ComputeDominantColor:  req.Parameters.ComputeDominantColor,
			FallbackDominantColor: req.Parameters.FallbackDominantColor,
		}
	}
	if req.Formats != nil {
		for _, format := range req.Formats {
			formats = append(formats, enums.ApiImageFormats[format])
		}
	}

	uploadResult, err := g.uploadSvc.Upload(ctx, upload.UploadImageParams{
		URL:          req.Url,
		ImageFormats: formats,
		Parameters:   parameters,
	})
	if err != nil {
		return nil, err
	}
	return &grpc_imageprocessor_v1.UploadResponse{
		Reference: uploadResult.Reference.String(),
		PublicUrl: uploadResult.PublicURL,
	}, nil
}

func NewGRPCService(
	db database.DB,
	uploadSvc upload.Service,
	filterSvc filter.Service,
) grpc_imageprocessor_v1.ImageProcessorServer {
	return &grpcService{
		db:        db,
		uploadSvc: uploadSvc,
		filterSvc: filterSvc,
	}
}
