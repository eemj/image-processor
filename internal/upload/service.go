package upload

import (
	"context"
	"reflect"

	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v9"
	"github.com/minio/minio-go/v7"
	"github.com/redis/go-redis/v9"
	"gitlab.com/eemj/image-processor/internal/analyzer"
	"gitlab.com/eemj/image-processor/internal/config/configmodels"
	"gitlab.com/eemj/image-processor/internal/convert"
	"gitlab.com/eemj/image-processor/internal/database"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	"gitlab.com/eemj/image-processor/internal/nats"
	"go.jamie.mt/logx/log"
	customconfig "go.jamie.mt/toolbox/configmodels"
	"go.opentelemetry.io/otel"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type service struct {
	tracer trace.Tracer
	logger *zap.Logger

	urlPrefixCfg configmodels.URLPrefix
	s3Cfg        customconfig.S3

	db database.DB

	analyzerSvc analyzer.Service

	redisSync *redsync.Redsync

	minioClient *minio.Client

	publisher nats.Publisher[*convert.ConvertAndUploadInput]
}

func (s service) L(ctx context.Context) *zap.Logger {
	return log.InjectActivity(ctx, s.logger)
}

type Service interface {
	BulkUpload(ctx context.Context, params BulkUploadImagesParams) (*BulkUploadImagesResult, error)
	Upload(ctx context.Context, param UploadImageParams) (*UploadImageResult, error)
	Settings(ctx context.Context) (*SettingsResult, error)
}

func NewService(
	urlPrefixCfg configmodels.URLPrefix,
	s3Cfg customconfig.S3,
	db database.DB,
	analyzerSvc analyzer.Service,
	minioClient *minio.Client,
	redisClient redis.UniversalClient,
	publisher nats.Publisher[*convert.ConvertAndUploadInput],
) Service {
	redisPool := goredis.NewPool(redisClient)
	redSync := redsync.New(redisPool)

	return &service{
		tracer: otel.Tracer(
			reflect.TypeFor[service]().PkgPath(),
			trace.WithInstrumentationAttributes(semconv.ServiceName(common.UploadServiceName)),
		),
		logger: log.Named(common.UploadServiceName),

		urlPrefixCfg: urlPrefixCfg,
		s3Cfg:        s3Cfg,

		db:          db,
		minioClient: minioClient,

		analyzerSvc: analyzerSvc,

		redisSync: redSync,

		publisher: publisher,
	}
}
