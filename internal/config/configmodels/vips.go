package configmodels

import (
	vips "github.com/davidbyttow/govips/v2/vips"
)

// Sourced from `github.com/davidbyttow/govips/v2/vips` govips.go
const (
	defaultConcurrencyLevel = 1
	defaultMaxCacheMem      = 50 * 1024 * 1024
	defaultMaxCacheSize     = 100
	defaultMaxCacheFiles    = 0
)

var DefaultVips = &Vips{
	CollectStats:       true,
	ReportLeaks:        true,
	ConcurrencyLevel:   defaultConcurrencyLevel,
	CacheMaximumMemory: defaultMaxCacheMem,
	CacheMaximumSize:   defaultMaxCacheSize,
	CacheMaximumFiles:  defaultMaxCacheFiles,
}

type Vips struct {
	ConcurrencyLevel   int  `mapstructure:"concurrency_level" json:"concurrency_level,omitempty"`
	CacheMaximumMemory int  `mapstructure:"cache_maximum_memory" json:"cache_maximum_memory,omitempty"`
	CacheMaximumSize   int  `mapstructure:"cache_maximum_size" json:"cache_maximum_size,omitempty"`
	CacheMaximumFiles  int  `mapstructure:"cache_maximum_files" json:"cache_maximum_files,omitempty"`
	ReportLeaks        bool `mapstructure:"report_leaks" json:"report_leaks,omitempty"`
	CollectStats       bool `mapstructure:"collect_stats" json:"collect_stats,omitempty"`
}

func (v *Vips) Validate() error {
	return nil
}

func (v *Vips) GoVips() *vips.Config {
	return &vips.Config{
		ConcurrencyLevel: v.ConcurrencyLevel,
		MaxCacheFiles:    v.CacheMaximumFiles,
		MaxCacheMem:      v.CacheMaximumMemory,
		MaxCacheSize:     v.CacheMaximumSize,
		ReportLeaks:      v.ReportLeaks,
		CollectStats:     v.CollectStats,
	}
}
