package config

import customconfig "go.jamie.mt/toolbox/configmodels"

type Migrate struct {
	Path string `json:"path" mapstructure:"path"`

	Database customconfig.PostgreSQL `json:"database" mapstructure:"database"`
}

func (c *Migrate) SetPath(path string)   { c.Path = path }
func (c *Migrate) Validate() (err error) { return c.Database.Validate() }
