package filter

import (
	"context"
	"net/url"
	"reflect"

	"github.com/jackc/pgx/v5"
	"github.com/segmentio/ksuid"
	"gitlab.com/eemj/image-processor/internal/config/configmodels"
	"gitlab.com/eemj/image-processor/internal/database"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	mapsutil "gitlab.com/eemj/image-processor/pkg/maps_util"
	"go.jamie.mt/logx/log"
	"go.opentelemetry.io/otel"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type (
	FilterParams struct {
		References []string `json:"references,omitempty"`
		URLs       []string `json:"url,omitempty"`
	}

	FilterResponse struct {
		Items []*domain.ImageFilterItem
	}
)

type Service interface {
	Filter(ctx context.Context, param FilterParams) (*FilterResponse, error)
}

type service struct {
	tracer       trace.Tracer
	logger       *zap.Logger
	urlPrefixCfg configmodels.URLPrefix
	db           database.DB
}

// Filter implements Service.
func (s *service) Filter(ctx context.Context, param FilterParams) (*FilterResponse, error) {
	ctx, span := s.tracer.Start(ctx, common.FilterServiceFilterSpan)
	defer span.End()

	requests, err := s.db.Q().FindImageRequest(ctx, sqlc.FindImageRequestParams{
		References: param.References,
		URLs:       param.URLs,
	})
	if err != nil && err != pgx.ErrNoRows {
		return nil, err
	}

	itemByReference := make(map[ksuid.KSUID]*domain.ImageFilterItem)
	for _, request := range requests {
		resultItem, exists := itemByReference[request.Reference]

		if !exists {
			// Ensure there's no funny business with the URL, use the native library to
			// join the URLs
			publicURL, err := url.JoinPath(s.urlPrefixCfg.ContentDelivery, request.Reference.String())
			if err != nil {
				return nil, err
			}

			// Create the base of the image, these details are shared amongst the images
			// dimensions will be the same for now since resize isn't implemented.
			resultItem = &domain.ImageFilterItem{
				Reference: request.Reference,
				OriginURL: request.URL,
				PublicURL: publicURL,
				Dimensions: &domain.ImageDimensions{
					Height: int64(request.Image.Height),
					Width:  int64(request.Image.Width),
				},
				CreatedAt: request.CreatedAt,
				UpdatedAt: request.UpdatedAt,
			}
		}

		// Every image is a different format, with different sizes & checksums.
		resultItem.Metadata = append(resultItem.Metadata, &domain.ImageMetadata{
			Format: request.Image.Format,
			Size:   request.Image.Size,
			Checksums: domain.ImageChecksums{
				SHA1: request.Image.SHA1,
			},
		})

		// Newest updated at
		if request.UpdatedAt.After(resultItem.UpdatedAt) {
			resultItem.UpdatedAt = request.UpdatedAt
		}
		// Oldest created at
		if request.CreatedAt.Before(resultItem.CreatedAt) {
			resultItem.CreatedAt = request.CreatedAt
		}

		itemByReference[request.Reference] = resultItem
	}
	return &FilterResponse{
		Items: mapsutil.Values(itemByReference),
	}, nil
}

func NewService(
	db database.DB,
	urlPrefixCfg configmodels.URLPrefix,
) Service {
	return &service{
		tracer: otel.Tracer(
			reflect.TypeFor[service]().PkgPath(),
			trace.WithInstrumentationAttributes(semconv.ServiceName(common.FilterServiceName)),
		),
		logger:       log.Named(common.FilterServiceName),
		db:           db,
		urlPrefixCfg: urlPrefixCfg,
	}
}
