package nats

import (
	"context"
	"encoding/json"
	"reflect"

	"gitlab.com/eemj/image-processor/internal/domain/common"
	"go.jamie.mt/logx/log"
	natsutil "go.jamie.mt/toolbox/nats_util"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type Publisher[T any] interface {
	Publish(ctx context.Context, payload T) (err error)
}

type publisher[T any] struct {
	tracer trace.Tracer
	logger *zap.Logger

	conn    *Connection
	subject string
}

func (p *publisher[T]) Publish(ctx context.Context, payload T) (err error) {
	natsMsg := natsutil.NewTraceMsg(ctx, p.subject)
	natsMsg.Data, err = json.Marshal(payload)
	if err != nil {
		return err
	}
	return p.conn.conn.PublishMsg(natsMsg)
}

func NewPublisher[T any](
	conn *Connection,
	subject string,
) Publisher[T] {
	return &publisher[T]{
		tracer: otel.Tracer(reflect.TypeFor[publisher[T]]().PkgPath()),
		logger: log.
			Named(common.NATSPublisherName).
			With(zap.String("subject", subject)),
		conn:    conn,
		subject: subject,
	}
}
