package build

import "runtime/debug"

const (
	EnvironmentReplacer = "IMAGE_PROCESSOR"
)

var (
	// Version is populated from the build script
	Version string = "devel"

	// GitSHA is the git commit sha version
	GitSHA string = ""
)

func init() {
	if GitSHA == "" {
		if info, ok := debug.ReadBuildInfo(); ok {
			for _, setting := range info.Settings {
				if setting.Key == "vcs.revision" {
					GitSHA = setting.Value
					break
				}
			}
		}
	}
}
