package convert

import "github.com/nats-io/nats.go/jetstream"

// FIXME(eemj): Not really a fan of how this is being configured..
// Ideally the names and the subjects are configured - but everything else should be constant.

const SubjectName = "convert"

// Configurations implements nats.ConsumeHandler.
func (c *consumerHandler) Configurations() (jetstream.StreamConfig, jetstream.ConsumerConfig) {
	return jetstream.StreamConfig{
			Name: "IMAGE_PROCESSOR_UPLOAD",
			Subjects: []string{
				SubjectName,
				(SubjectName + ".*"),
			},
			Discard:     jetstream.DiscardOld,
			Storage:     jetstream.FileStorage,
			Retention:   jetstream.WorkQueuePolicy,
			AllowDirect: true,
			AllowRollup: true,
			NoAck:       false,
		}, jetstream.ConsumerConfig{
			Name:      "IMAGE_PROCESSOR_CONSUMER",
			Durable:   "IMAGE_PROCESSOR_CONSUMER",
			AckPolicy: jetstream.AckExplicitPolicy,
			FilterSubjects: []string{
				SubjectName,
			},
		}
}
