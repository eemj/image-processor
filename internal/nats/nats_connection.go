package nats

import (
	"reflect"

	"github.com/nats-io/nats.go"
	"gitlab.com/eemj/image-processor/internal/config/configmodels"
	"go.jamie.mt/logx/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type Connection struct {
	tracer trace.Tracer
	logger *zap.Logger
	conn   *nats.Conn
}

func NewConnection(cfg configmodels.NATSConnectionConfig) (m *Connection, err error) {
	n := &Connection{
		tracer: otel.Tracer(reflect.TypeFor[Connection]().PkgPath()),
		logger: log.Named("nats"),
	}

	n.conn, err = nats.Connect(cfg.URL, append(
		cfg.Options(),
		nats.ConnectHandler(n.connectHandler),
		nats.DisconnectErrHandler(n.disconnectErrHandler),
		nats.ErrorHandler(n.errorHandler),
		nats.ReconnectHandler(n.reconnectHandler),
	)...)
	if err != nil {
		return nil, err
	}
	return n, nil
}

func (n *Connection) Close() {
	if n.conn != nil {
		n.conn.Close()
	}
}
