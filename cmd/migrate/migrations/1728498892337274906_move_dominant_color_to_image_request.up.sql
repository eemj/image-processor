alter table image_request
    add column if not exists dominant_color text null;

update image_request
set dominant_color = image.dominant_color
from image
where image.dominant_color is not null and image.image_request_id = image_request.id;

alter table image
    drop column if exists dominant_color;
