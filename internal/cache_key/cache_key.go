package cachekey

const (
	// BaseKeyPrefix resembles the cache prefix to be used
	// for distributed systems.
	BaseKeyPrefix = "image-processor"

	// KeyPrefix resembles the cache prefix to be used
	// for distributed systems.
	KeyPrefix = BaseKeyPrefix + ":"
)

// UploadHash returns "`image-processor`:`upload-hash`:`<hash>`"
func UploadHash(hash string) string {
	return KeyPrefix + "upload-hash:" + hash
}
