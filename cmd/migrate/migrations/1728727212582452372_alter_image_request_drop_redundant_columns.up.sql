alter table image_request
    drop column if exists parameters,
    drop column if exists formats;