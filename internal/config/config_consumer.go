package config

import (
	"errors"

	"gitlab.com/eemj/image-processor/internal/config/configmodels"
	customconfig "go.jamie.mt/toolbox/configmodels"
)

type Consumer struct {
	Vips *configmodels.Vips                 `json:"vips" mapstructure:"vips"`
	NATS *configmodels.NATSConnectionConfig `json:"nats" mapstructure:"nats"`

	Database  customconfig.PostgreSQL `json:"database" mapstructure:"database"`
	Telemetry customconfig.Telemetry  `json:"telemetry" mapstructure:"telemetry"`
	Logging   customconfig.Logging    `json:"logging" mapstructure:"logging"`
	S3        customconfig.S3         `json:"s3" mapstructure:"s3"`

	Path string `json:"-" mapstructure:"-"`
}

func (c *Consumer) SetPath(path string) { c.Path = path }

func (c *Consumer) Validate() (err error) {
	err = errors.Join(
		c.Database.Validate(),
		c.Telemetry.Validate(),
		c.Logging.Validate(),
		c.S3.Validate(),
	)
	if c.NATS == nil {
		err = errors.Join(err, errors.New("`nats` is required"))
	} else {
		err = errors.Join(err, c.NATS.Validate())
	}
	if c.Vips == nil { // It's optional - if it's not provided, use defaults
		c.Vips = configmodels.DefaultVips
	} else {
		err = errors.Join(err, c.Vips.Validate())
	}
	return err
}
