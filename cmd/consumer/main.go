package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"

	"github.com/davidbyttow/govips/v2/vips"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"gitlab.com/eemj/image-processor/internal/analyzer"
	"gitlab.com/eemj/image-processor/internal/build"
	"gitlab.com/eemj/image-processor/internal/config"
	"gitlab.com/eemj/image-processor/internal/convert"
	"gitlab.com/eemj/image-processor/internal/database"
	"gitlab.com/eemj/image-processor/internal/nats"
	vips_util "gitlab.com/eemj/image-processor/pkg/vips_util"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/configparser"
	"go.jamie.mt/toolbox/telemetry"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.uber.org/automaxprocs/maxprocs"
	"go.uber.org/zap"
)

const (
	ApplicationName = "image-processor-consumer"
)

func main() {
	log.Create(zap.InfoLevel, log.ConsoleCore)

	// AutoMaxProcs
	maxprocs.Set(maxprocs.Logger(log.Named("automaxprocs").Sugar().Infof))

	log.Infow(
		"build",
		zap.String("version", build.Version),
		zap.String("git_sha", build.GitSHA),
	)

	// Config & Logging
	cfg, err := configparser.Parse[*config.Consumer](configparser.ParserOptions{
		ApplicationName:      ApplicationName,
		EnvironmentReplacer:  build.EnvironmentReplacer,
		EnvironmentDelimiter: configparser.DefaultEnvironmentDelimiter,
		ConfigDelimiter:      configparser.DefaultConfigDelimiter,
		Filename:             configparser.DefaultFilename,
	})
	if err != nil {
		log.Fatal(err)
	}

	coreBuilders := []log.CoreBuilder{log.ConsoleCore}
	if cfg.Logging.File.Enable {
		if writeSync := cfg.Logging.ZapWriteSync(); writeSync != nil {
			coreBuilders = append(coreBuilders, log.JSONCore(writeSync))
		}
	}

	log.Create(cfg.Logging.ZapLevel(), coreBuilders...)

	// VIPS
	vips.LoggingSettings(
		vips_util.LoggingSettings,
		vips_util.ZapToVipsLoggingLevel(cfg.Logging.ZapLevel()),
	)
	vips.Startup(cfg.Vips.GoVips())
	defer vips.Shutdown()

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()

	db, err := database.NewDB(cfg.Database)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// OpenTelemetry
	if cfg.Telemetry.Enable {
		provider, err := telemetry.NewTraceProvider(
			ctx,
			cfg.Telemetry,
			semconv.ServiceNameKey.String(ApplicationName),
			semconv.ServiceVersionKey.String(build.Version+`-`+build.GitSHA),
		)
		if err != nil {
			log.Fatal(err)
		}
		defer func(provider *trace.TracerProvider, ctx context.Context) {
			err := provider.Shutdown(ctx)
			if err != nil {
				log.Errorw("failed to shutdown trace provider", zap.Error(err))
			}
		}(provider, context.Background())
	}

	// MinIO
	minioOptions := &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.S3.AccessKey, cfg.S3.SecretKey, ""),
		Secure: cfg.S3.UseSSL,
		Region: cfg.S3.Region,
	}
	if cfg.Telemetry.Enable {
		minioTransport, err := minio.DefaultTransport(cfg.S3.UseSSL)
		if err != nil {
			log.Fatal(err)
		}
		minioOptions.Transport = otelhttp.NewTransport(
			minioTransport,
			otelhttp.WithMessageEvents(otelhttp.ReadEvents, otelhttp.WriteEvents),
			otelhttp.WithSpanNameFormatter(func(operation string, r *http.Request) string {
				if r.URL.Host == "" {
					return "HTTP " + r.Method
				}
				return "HTTP (" + r.URL.Host + ") " + r.Method
			}),
		)
	}
	minioClient, err := minio.New(cfg.S3.Endpoint, minioOptions)
	if err != nil {
		log.Fatal(err)
	}

	natsConn, err := nats.NewConnection(*cfg.NATS)
	if err != nil {
		log.Fatal(err)
	}
	defer natsConn.Close()

	// Services
	analyzerSvc := analyzer.NewService(db, minioClient, cfg.S3)

	convertConsumeHandler := convert.NewConsumerHandler(db, minioClient, cfg.S3, analyzerSvc)
	convertConsumer, err := nats.NewConsumer(natsConn, convertConsumeHandler)
	if err != nil {
		log.Fatal(err)
	}

	pprofAddr := os.Getenv("PPROF_ADDR")
	if pprofAddr != "" {
		go func() {
			if err := http.ListenAndServe(pprofAddr, nil); err != nil {
				log.Fatal(err)
			}
		}()
	}

	err = convertConsumer.Consume(ctx)
	if err != nil {
		log.Fatal(err)
	}
}
