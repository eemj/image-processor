package vipsutil

import (
	"strings"

	"github.com/davidbyttow/govips/v2/vips"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func LoggingSettings(domain string, vipsLevel vips.LogLevel, message string) {
	var (
		fields []zap.Field
		logger = log.Named("govips").With(zap.String("domain", domain))
		level  zapcore.Level
	)
	switch vipsLevel {
	case vips.LogLevelError:
		level = zap.ErrorLevel
	case vips.LogLevelCritical:
		level = zap.ErrorLevel
		fields = append(fields, zap.Namespace("critical"))
	case vips.LogLevelWarning:
		level = zap.WarnLevel
	case vips.LogLevelInfo, vips.LogLevelMessage:
		level = zap.InfoLevel
	case vips.LogLevelDebug:
		level = zap.DebugLevel
	}

	logger.Log(level, strings.TrimSpace(message), fields...)
}

func ZapToVipsLoggingLevel(lvl zapcore.Level) vips.LogLevel {
	switch lvl {
	case zap.InfoLevel:
		return vips.LogLevelInfo
	case zap.DebugLevel:
		return vips.LogLevelDebug
	case zap.ErrorLevel:
		return vips.LogLevelError
	case zap.FatalLevel, zap.DPanicLevel:
		return vips.LogLevelCritical
	case zap.WarnLevel:
		return vips.LogLevelWarning
	}
	return vips.LogLevelMessage
}
