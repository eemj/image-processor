package convert

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"strconv"

	"github.com/davidbyttow/govips/v2/vips"
	"github.com/google/uuid"
	"github.com/minio/minio-go/v7"
	"gitlab.com/eemj/image-processor/internal/analyzer"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/bitflags"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
	"go.uber.org/zap"
)

type (
	ConvertImageInput struct {
		ImageRequestID uuid.UUID         `json:"image_request_id,omitempty"`
		ImageFormat    enums.ImageFormat `json:"format,omitempty"`
	}
	ConvertImageReply struct {
		ImageFormat enums.ImageFormat `json:"image_format,omitempty"`
		ImageID     uuid.UUID         `json:"image_id,omitempty"`
		ObjectKey   string            `json:"object_key,omitempty"`
	}
)

func (c *consumerHandler) convertImage(ctx context.Context, input *ConvertImageInput) (*ConvertImageReply, error) {
	pendingResult, err := c.analyzerSvc.Pending(ctx, analyzer.AnalyzerPendingParams{
		ImageRequestID: &input.ImageRequestID,
	})
	if err != nil {
		return nil, err
	}

	var (
		pendingImage  *analyzer.AnalyzerPendingImageFormat
		originalImage *domain.Image
	)
	for _, currentPendingImage := range pendingResult.Formats {
		if currentPendingImage.Image != nil &&
			bitflags.Has(currentPendingImage.Image.Flags, bitflags.IMAGE_FLAG_ORIGINAL) {
			originalImage = currentPendingImage.Image
		}

		if currentPendingImage.Format == input.ImageFormat {
			pendingImage = currentPendingImage
		}

		if pendingImage != nil && originalImage != nil {
			break
		}
	}

	if pendingImage == nil {
		pendingImage = &analyzer.AnalyzerPendingImageFormat{
			Format:   input.ImageFormat,
			Image:    nil,
			S3:       false, // Set to `false` as they're all pending
			Database: false,
		}
	}

	if originalImage == nil {
		return nil, ErrOriginalImageInRequestIDNotFound(pendingResult.ImageRequest.ID)
	}

	// everything else is processed already
	if (pendingImage.Database && pendingImage.S3) ||
		(input.ImageFormat == originalImage.Format) {
		return &ConvertImageReply{
			ImageFormat: input.ImageFormat,
			ImageID:     pendingImage.Image.ID,
			ObjectKey:   pendingImage.Image.ObjectKey,
		}, nil
	}

	c.logger.Info(
		"retrieving original image",
		zap.String("object_key", originalImage.ObjectKey),
	)

	objectInfo, err := c.minioClient.GetObject(
		ctx,
		c.s3Config.Bucket,
		originalImage.ObjectKey,
		minio.GetObjectOptions{Checksum: false},
	)
	if err != nil {
		return nil, err
	}
	defer objectInfo.Close()

	c.logger.Info(
		"converting images",
		zap.Stringer("from", originalImage.Format),
		zap.Stringer("to", input.ImageFormat),
		zap.Stringer("reference", originalImage.Reference),
	)

	sha1Hash := sha1.New()
	vipsRef, err := vips.NewImageFromReader(
		io.TeeReader(objectInfo, sha1Hash), // object | sha1
	)
	if err != nil {
		return nil, err
	}
	defer vipsRef.Close()

	vipsExportParams := vips.NewDefaultExportParams()
	vipsExportParams.Format = input.ImageFormat.Vips()
	vipsExportParams.Lossless = true

	var (
		exportBytes []byte
		metadata    *vips.ImageMetadata
	)
	switch input.ImageFormat {
	case enums.IMAGE_FORMAT_AVIF:
		exportBytes, metadata, err = vipsRef.ExportAvif(vips.NewAvifExportParams())
	case enums.IMAGE_FORMAT_JPEG:
		exportBytes, metadata, err = vipsRef.ExportJpeg(vips.NewJpegExportParams())
	case enums.IMAGE_FORMAT_PNG:
		exportBytes, metadata, err = vipsRef.ExportPng(vips.NewPngExportParams())
	case enums.IMAGE_FORMAT_WEBP:
		exportBytes, metadata, err = vipsRef.ExportWebp(vips.NewWebpExportParams())
	default:
		err = fmt.Errorf("unsupported image format '%s'", input.ImageFormat)
	}
	if err != nil {
		return nil, err
	}

	var (
		imageSize   = int64(len(exportBytes))
		imageHeight = metadata.Height
		imageWidth  = metadata.Width
		objectKey   = common.CreateObjectKey(pendingResult.ImageRequest.Reference, input.ImageFormat)
	)

	if !pendingImage.S3 {
		c.logger.Debug(
			"uploading converted image to object store",
			zap.String("object_key", objectKey),
		)

		_, err = c.minioClient.PutObject(
			ctx,
			c.s3Config.Bucket,
			objectKey,
			bytes.NewReader(exportBytes),
			imageSize,
			minio.PutObjectOptions{
				ContentType: input.ImageFormat.Mime(),
				UserMetadata: map[string]string{
					common.MetadataImageHeight: strconv.Itoa(imageHeight),
					common.MetadataImageWidth:  strconv.Itoa(imageWidth),
				},
			},
		)
		if err != nil {
			return nil, err
		}
	}

	var imageID uuid.UUID
	if !pendingImage.Database {
		sha1Hex := hex.EncodeToString(sha1Hash.Sum(nil))

		c.logger.Debug(
			"storing converted image to entity store",
			zap.String("hash", sha1Hex),
		)

		imageID, err = c.db.Q().InsertImage(ctx, sqlc.InsertImageParams{
			ImageRequestID: pendingResult.ImageRequest.ID,
			Reference:      pendingResult.ImageRequest.Reference,
			ObjectKey:      objectKey,
			Size:           imageSize,
			Height:         int32(imageHeight),
			Width:          int32(imageWidth),
			Format:         input.ImageFormat,
			SHA1:           sha1Hex,
			Flags:          bitflags.IMAGE_FLAG_NONE,
		})
		if err != nil {
			return nil, err
		}
	} else {
		imageID = pendingImage.Image.ID
	}

	return &ConvertImageReply{
		ImageFormat: input.ImageFormat,
		ObjectKey:   objectKey,
		ImageID:     imageID,
	}, nil
}
