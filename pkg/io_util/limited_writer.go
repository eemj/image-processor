package ioutil

func LimitBuffer(n int64) *LimitedBuffer {
	return &LimitedBuffer{B: make([]byte, n), L: n, N: 0}
}

type LimitedBuffer struct {
	B []byte
	L int64
	N int64
}

func (lb *LimitedBuffer) Write(p []byte) (n int, err error) {
	if lb.N >= lb.L {
		return len(p), nil
	}
	pl := len(p)
	if int64(pl) > lb.L-lb.N {
		p = p[:lb.L-lb.N]
	}
	lb.N += int64(copy(lb.B[lb.N:], p))
	return pl, nil
}

func (lb *LimitedBuffer) Reset() {
	lb.B = nil
	lb.N = 0
}
