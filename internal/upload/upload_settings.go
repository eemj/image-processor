package upload

import (
	"context"

	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/eemj/image-processor/internal/domain/common"
)

type SettingsResult struct {
	ContentDeliveryBaseURL string `json:"content_delivery_url,omitempty"`
}

func (s *service) Settings(ctx context.Context) (result *SettingsResult, err error) {
	_, span := s.tracer.Start(ctx, common.UploadServiceSettingsSpan, trace.WithAttributes(
		semconv.ServiceName(common.UploadServiceName),
	))
	defer span.End()

	return &SettingsResult{
		ContentDeliveryBaseURL: s.urlPrefixCfg.ContentDelivery,
	}, nil
}
