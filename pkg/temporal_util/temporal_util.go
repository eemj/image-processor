package temporalutil

import (
	"fmt"
	"strings"

	"go.jamie.mt/logx/fields"
	"go.temporal.io/sdk/log"
	"go.uber.org/zap"
)

type ZapLogger struct{ Logger *zap.Logger }

// Source: https://stackoverflow.com/questions/56616196/how-to-convert-camel-case-string-to-snake-case`
func ToSnake(camel string) (snake string) {
	var b strings.Builder
	diff := 'a' - 'A'
	l := len(camel)
	for i, v := range camel {
		// A is 65, a is 97
		if v >= 'a' {
			b.WriteRune(v)
			continue
		}
		// v is capital letter here
		// irregard first letter
		// add underscore if last letter is capital letter
		// add underscore when previous letter is lowercase
		// add underscore when next letter is lowercase
		if (i != 0 || i == l-1) && (          // head and tail
		(i > 0 && rune(camel[i-1]) >= 'a') || // pre
			(i < l-1 && rune(camel[i+1]) >= 'a')) { // next
			b.WriteRune('_')
		}
		b.WriteRune(v + diff)
	}
	return b.String()
}

func (log *ZapLogger) fields(keyvals []any) []zap.Field {
	if len(keyvals)%2 != 0 {
		return []zap.Field{zap.Error(fmt.Errorf("odd number of keyvals pairs: %v", keyvals))}
	}

	zapFields := make([]zap.Field, 0, (len(keyvals) / 2))
	for i := 0; i < len(keyvals); i += 2 {
		key, ok := keyvals[i].(string)
		if !ok {
			key = fmt.Sprintf("%v", keyvals[i])
		}
		key = ToSnake(key)
		switch key {
		case "trace_id":
			key = fields.ActivityTraceIDKey
		case "span_id":
			key = fields.ActivitySpanIDKey
		}
		zapFields = append(zapFields, zap.Any(key, keyvals[i+1]))
	}

	return zapFields
}

func (log *ZapLogger) Debug(msg string, keyvals ...any) {
	log.Logger.Debug(msg, log.fields(keyvals)...)
}

func (log *ZapLogger) Info(msg string, keyvals ...any) {
	log.Logger.Info(msg, log.fields(keyvals)...)
}

func (log *ZapLogger) Warn(msg string, keyvals ...any) {
	log.Logger.Warn(msg, log.fields(keyvals)...)
}

func (log *ZapLogger) Error(msg string, keyvals ...any) {
	log.Logger.Error(msg, log.fields(keyvals)...)
}

func (log *ZapLogger) With(keyvals ...any) log.Logger {
	return &ZapLogger{
		Logger: log.Logger.With(log.fields(keyvals)...),
	}
}

func (log *ZapLogger) WithCallerSkip(skip int) log.Logger {
	return &ZapLogger{
		Logger: log.Logger.WithOptions(zap.AddCallerSkip(skip)),
	}
}
