package upload

import (
	"context"
	"errors"
	"net/url"
	"slices"
	"strconv"

	"github.com/go-redsync/redsync/v4"
	"github.com/google/uuid"
	"github.com/minio/minio-go/v7"
	"github.com/segmentio/ksuid"
	"gitlab.com/eemj/image-processor/internal/analyzer"
	cachekey "gitlab.com/eemj/image-processor/internal/cache_key"
	"gitlab.com/eemj/image-processor/internal/convert"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/bitflags"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
	stringutil "go.jamie.mt/toolbox/string_util"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UploadImageParams struct {
	ImageFormats []enums.ImageFormat           `json:"image_formats,omitempty"`
	Parameters   domain.ImageRequestParameters `json:"parameters,omitempty"`
	URL          string                        `json:"raw_url,omitempty"`
}

func (p *UploadImageParams) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(p.URL) {
		err = errors.Join(err, status.Error(codes.InvalidArgument, "`url` is required"))
	}

	_, parsedErr := url.Parse(p.URL)
	if parsedErr != nil {
		err = errors.Join(err, status.Errorf(codes.InvalidArgument, "`url` failed to parse due to `%v`", parsedErr))
	}

	if len(p.ImageFormats) == 0 {
		err = errors.Join(err, status.Error(codes.InvalidArgument, "`image_formats` is required"))
	}

	for _, format := range p.ImageFormats {
		if format.Valid() {
			continue
		}

		err = errors.Join(err, status.Errorf(codes.InvalidArgument, "`image_formats` has an invalid value `%s`", format))
	}

	return err
}

type UploadImageResult struct {
	PublicURL      string      `json:"public_url,omitempty"`
	ImageRequestID uuid.UUID   `json:"image_request_id,omitempty"`
	Reference      ksuid.KSUID `json:"reference,omitempty"`
}

func (s *service) Upload(ctx context.Context, param UploadImageParams) (*UploadImageResult, error) {
	ctx, span := s.tracer.Start(ctx, common.UploadServiceUploadSpan)
	defer span.End()

	err := param.Validate()
	if err != nil {
		return nil, err
	}

	prepareResult, err := s.analyzerSvc.Prepare(ctx, analyzer.AnalyzerPrepareParams{
		URL: param.URL,
	})
	if err != nil {
		s.L(ctx).Error("failed to analyze requested image", zap.Error(err))
		return nil, err
	}

	redSyncKey := cachekey.UploadHash(prepareResult.SHA1)
	mutex := s.redisSync.NewMutex(redSyncKey, redsync.WithFailFast(true))
	mutex.LockContext(ctx)
	defer mutex.UnlockContext(ctx)

	s.L(ctx).Debug(
		"redlocking this image hash",
		zap.String("key", redSyncKey),
		zap.String("sha1", prepareResult.SHA1),
	)

	var (
		imageRequestID *uuid.UUID
		imageFormats   = param.ImageFormats
		reference      string
	)
	if !slices.Contains(imageFormats, prepareResult.ImageFormat) {
		s.L(ctx).Debug(
			"original `format` is not part of the requested `formats`",
			zap.Stringer("format", prepareResult.ImageFormat),
			zap.Stringers("formats", imageFormats),
		)

		imageFormats = append(imageFormats, prepareResult.ImageFormat)
	}
	pendingImageFormats := slices.Clone(imageFormats)
	reference = prepareResult.Reference.String()
	publicURL, err := url.JoinPath(s.urlPrefixCfg.ContentDelivery, reference)
	if err != nil {
		return nil, err
	}

	if prepareResult.ImageRequest != nil {
		imageRequestID = &prepareResult.ImageRequest.ID

		s.L(ctx).Info(
			"existing image request was found",
			zap.Stringer("image_request_id", imageRequestID),
		)

		// if they're not the same length, filter the image request images to
		// what we want.
		if len(imageFormats) != len(prepareResult.ImageRequest.Images) {
			images := make([]*domain.Image, 0, len(imageFormats))
			for _, image := range prepareResult.ImageRequest.Images {
				if slices.Contains(imageFormats, image.Format) {
					images = append(images, image)
				}
			}
			prepareResult.ImageRequest.Images = images
		}

		pendingResult, err := s.analyzerSvc.Pending(ctx, analyzer.AnalyzerPendingParams{
			ImageRequest:    prepareResult.ImageRequest,
			ImageFormats:    imageFormats,
			ImageParameters: &param.Parameters,
		})
		if err != nil {
			s.L(ctx).Error("failed to analyze what's pending", zap.Error(err))
			return nil, err
		}

		zeroActions := pendingResult.DominantColor

		s.L(ctx).Info(
			"availability check",
			zap.Stringer("reference", prepareResult.Reference),
			zap.Bool("actioned", pendingResult.DominantColor),
			zap.Dict(
				"processes",
				zap.Dict("image_request",
					zap.Bool("dominant_color", pendingResult.DominantColor),
				),
			),
		)

		for _, format := range pendingResult.Formats {
			actioned := (format.Database && format.S3)

			s.L(ctx).Info(
				"availability check",
				zap.Stringer("reference", prepareResult.Reference),
				zap.Bool("actioned", actioned),
				zap.Dict(
					"processes",
					zap.Dict("image",
						zap.Bool("database", format.Database),
						zap.Bool("s3", format.S3),
					),
				))

			if actioned {
				// remove this format since there are no actions
				pendingImageFormats = slices.DeleteFunc(
					pendingImageFormats,
					func(f enums.ImageFormat) bool { return f == format.Format },
				)
			}

			zeroActions = zeroActions && actioned
		}

		// everything is processed
		if zeroActions {
			s.L(ctx).Debug(
				"there are no pending actions left",
				zap.Stringer("image_request_id", imageRequestID),
			)

			return &UploadImageResult{
				PublicURL:      publicURL,
				ImageRequestID: *imageRequestID,
				Reference:      prepareResult.Reference,
			}, nil
		}
	}

	if imageRequestID == nil {
		createdImageRequestID, err := s.db.Q().InsertImageRequest(ctx, sqlc.InsertImageRequestParams{
			Reference: prepareResult.Reference,
			URL:       param.URL,
			SHA1:      prepareResult.SHA1,
			Status:    enums.IMAGE_REQUEST_STATUS_NEW,
		})
		if err != nil {
			s.L(ctx).Error("failed to insert image request", zap.Error(err))
			return nil, err
		}
		imageRequestID = &createdImageRequestID

		objectKey := common.CreateObjectKey(prepareResult.Reference, prepareResult.ImageFormat)

		// immediately upload it to S3
		uploadInfo, err := s.minioClient.PutObject(
			ctx,
			s.s3Cfg.Bucket,
			objectKey,
			prepareResult.Buffer,
			int64(prepareResult.Buffer.Len()),
			minio.PutObjectOptions{
				ContentType: prepareResult.ImageFormat.Mime(),
				UserMetadata: map[string]string{
					common.MetadataImageHeight: strconv.FormatInt(prepareResult.ImageSize.Height, 10),
					common.MetadataImageWidth:  strconv.FormatInt(prepareResult.ImageSize.Width, 10),
				},
			},
		)
		if err != nil {
			s.L(ctx).Error("failed to put original image in s3", zap.Error(err))
			return nil, err
		}

		imageID, err := s.db.Q().InsertImage(ctx, sqlc.InsertImageParams{
			ImageRequestID: *imageRequestID,
			Reference:      prepareResult.Reference,
			ObjectKey:      objectKey,
			Size:           uploadInfo.Size,
			Height:         int32(prepareResult.ImageSize.Height),
			Width:          int32(prepareResult.ImageSize.Width),
			Format:         prepareResult.ImageFormat,
			SHA1:           prepareResult.SHA1,
			Flags:          bitflags.IMAGE_FLAG_ORIGINAL,
		})
		if err != nil {
			s.L(ctx).Error("failed to insert original image", zap.Error(err))
			return nil, err
		}

		s.L(ctx).Info(
			"created entries",
			zap.Stringer("image_request_id", imageRequestID),
			zap.Stringer("image_id", imageID),
			zap.String("object_key", objectKey),
		)
	}

	// updating the `image_request` status to 'queued'
	err = s.db.Q().UpdateImageRequest(ctx, sqlc.UpdateImageRequestParams{
		ID:     *imageRequestID,
		Status: enums.IMAGE_REQUEST_STATUS_QUEUED.Enum(),
	})
	if err != nil {
		s.L(ctx).Error(
			"failed to change status for existing image request",
			zap.Error(err),
		)
		return nil, err
	}

	err = s.publisher.Publish(ctx, &convert.ConvertAndUploadInput{
		ImageRequestID:  *imageRequestID,
		ImageFormats:    pendingImageFormats,
		ImageParameters: param.Parameters,
	})
	if err != nil {
		s.L(ctx).Error("failed to publish convert and upload message", zap.Error(err))
		return nil, err
	}
	s.L(ctx).Info(
		"enqueued message",
		zap.Stringers("formats", pendingImageFormats),
		zap.Any("parameters", param.Parameters),
	)

	return &UploadImageResult{
		PublicURL:      publicURL,
		ImageRequestID: *imageRequestID,
		Reference:      prepareResult.Reference,
	}, nil
}
