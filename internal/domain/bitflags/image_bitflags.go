package bitflags

type ImageFlags uint

const (
	IMAGE_FLAG_NONE ImageFlags = iota
	IMAGE_FLAG_ORIGINAL
)
