package configmodels

import (
	"errors"
	"net"
	"strconv"
	"strings"
	"time"

	stringutil "go.jamie.mt/toolbox/string_util"
)

type Metrics struct {
	Host      string        `json:"host" mapstructure:"host"`
	Port      int           `json:"port" mapstructure:"port"`
	RoutePath string        `json:"routePath" mapstructure:"route_path"`
	Timeout   time.Duration `json:"timeout" mapstructure:"timeout"`
}

func (m Metrics) HostPort() string {
	return net.JoinHostPort(
		m.Host,
		strconv.FormatUint(uint64(m.Port), 10),
	)
}

func (m *Metrics) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(m.Host) {
		m.Host = "0.0.0.0"
	}

	if m.Port < 0 || m.Port > 65535 {
		err = errors.Join(err, errors.New("`metrics.port` must be between 0 and 65535"))
	} else if m.Port == 0 {
		m.Port = 9800 // Default port is `9800`
	}

	if stringutil.IsTrimmedEmpty(m.RoutePath) {
		m.RoutePath = "/metrics"
	}
	if !strings.HasPrefix(m.RoutePath, "/") {
		m.RoutePath = ("/" + m.RoutePath)
	}

	if m.Timeout < 0 {
		err = errors.Join(err, errors.New("`metrics.timeout` must be greater than 0"))
	} else if m.Timeout == 0 {
		m.Timeout = 30 * time.Second // Default to 30 seconds
	}

	return err
}
