package domain

import (
	"errors"
	"fmt"
	"strings"
)

type ImageRequestParameters struct {
	ComputeDominantColor  bool    `json:"compute_dominant_color"`
	FallbackDominantColor *string `json:"fallback_dominant_color,omitempty"`
}

func (h ImageRequestParameters) Validate() (err error) {
	if h.ComputeDominantColor && h.FallbackDominantColor != nil && *h.FallbackDominantColor != "" {
		fallbackDominantColor := strings.TrimFunc(
			strings.ToLower(*h.FallbackDominantColor),
			func(r rune) bool { return r == '#' || r == ' ' },
		)
		for index, char := range fallbackDominantColor {
			switch {
			case char >= '0' && char <= '9',
				char >= 'a' && char <= 'f':
				continue
			}
			err = errors.Join(fmt.Errorf("`parameters.fallback_dominant_color` character at index '%d' is invalid", index))
		}
	}
	if err != nil {
		return err
	}
	return nil
}
