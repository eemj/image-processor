package mapsutil

func Swap[K, V comparable](m1 map[K]V) map[V]K {
	m2 := make(map[V]K, len(m1))
	for k, v := range m1 {
		m2[v] = k
	}
	return m2
}

func Values[K comparable, V any](m1 map[K]V) []V {
	m2 := make([]V, 0, len(m1))
	for _, v := range m1 {
		m2 = append(m2, v)
	}
	return m2
}
