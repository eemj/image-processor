package main

import (
	"embed"
	"flag"

	"gitlab.com/eemj/image-processor/internal/build"
	"gitlab.com/eemj/image-processor/internal/config"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/configparser"
	migrateutil "go.jamie.mt/toolbox/migrate_util"
	"go.uber.org/zap"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	_ "github.com/lib/pq"
)

const (
	ApplicationName string = "image-processor-migrate"
)

var (
	//go:embed migrations/*.sql
	migrationFs embed.FS

	// flags
	down = false
)

func main() {
	log.Create(zap.InfoLevel, log.ConsoleCore)

	flag.BoolVar(&down, "down", false, "drop everything")
	flag.Parse()

	log.Infow(
		"build",
		zap.String("version", build.Version),
		zap.String("git_sha", build.GitSHA),
	)

	cfg, err := configparser.Parse[*config.Migrate](configparser.ParserOptions{
		ApplicationName:      ApplicationName,
		EnvironmentReplacer:  build.EnvironmentReplacer,
		EnvironmentDelimiter: configparser.DefaultEnvironmentDelimiter,
		ConfigDelimiter:      configparser.DefaultConfigDelimiter,
		Filename:             configparser.DefaultFilename,
	})
	if err != nil {
		log.Fatal(err)
	}

	driver, err := iofs.New(migrationFs, "migrations")
	if err != nil {
		log.Fatal(err)
	}
	defer driver.Close()

	log.Named("database").
		Info(
			"connection",
			zap.String("url", cfg.Database.String()),
		)

	migrator, err := migrate.NewWithSourceInstance(
		"iofs",
		driver,
		cfg.Database.UnsafeString(),
	)
	if err != nil {
		log.Fatal(err)
	}

	defer migrator.Close()

	migrator.Log = migrateutil.NewLogger(log.Named("migrate"))

	if down {
		err = migrator.Down()
	} else {
		err = migrator.Up()
	}

	if err != nil && err != migrate.ErrNoChange {
		log.Fatal(err)
	}
}
