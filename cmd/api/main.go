package main

import (
	"context"
	"os"
	"os/signal"

	"github.com/davidbyttow/govips/v2/vips"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/redis/go-redis/v9"
	grpc_imageprocessor_v1 "gitlab.com/eemj/image-processor/api/grpc_imageprocessor/v1"
	"gitlab.com/eemj/image-processor/internal/analyzer"
	"gitlab.com/eemj/image-processor/internal/build"
	"gitlab.com/eemj/image-processor/internal/config"
	"gitlab.com/eemj/image-processor/internal/convert"
	"gitlab.com/eemj/image-processor/internal/database"
	"gitlab.com/eemj/image-processor/internal/filter"
	image_processor "gitlab.com/eemj/image-processor/internal/image_processor"
	"gitlab.com/eemj/image-processor/internal/nats"
	"gitlab.com/eemj/image-processor/internal/upload"
	vips_util "gitlab.com/eemj/image-processor/pkg/vips_util"
	logger_grpc "go.jamie.mt/logx/interceptors/logging"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/configparser"
	grpc_util "go.jamie.mt/toolbox/grpc_util"
	redis_util "go.jamie.mt/toolbox/redis_util"
	"go.jamie.mt/toolbox/telemetry"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
	"go.uber.org/automaxprocs/maxprocs"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/reflection"
)

const (
	ApplicationName string = "image-processor-api"
)

func main() {
	maxprocs.Set(maxprocs.Logger(log.Named("automaxprocs").Sugar().Infof))

	log.Create(zap.DebugLevel, log.ConsoleCore)

	log.Infow(
		"build",
		zap.String("version", build.Version),
		zap.String("git_sha", build.GitSHA),
	)

	// Config & Logging
	cfg, err := configparser.Parse[*config.API](configparser.ParserOptions{
		ApplicationName:      ApplicationName,
		EnvironmentReplacer:  build.EnvironmentReplacer,
		EnvironmentDelimiter: configparser.DefaultEnvironmentDelimiter,
		ConfigDelimiter:      configparser.DefaultConfigDelimiter,
		Filename:             configparser.DefaultFilename,
	})
	if err != nil {
		log.Fatal(err)
	}

	coreBuilders := []log.CoreBuilder{log.ConsoleCore}
	if cfg.Logging.File.Enable {
		if writeSync := cfg.Logging.ZapWriteSync(); writeSync != nil {
			coreBuilders = append(coreBuilders, log.JSONCore(writeSync))
		}
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	log.Create(cfg.Logging.ZapLevel(), coreBuilders...)

	// VIPS
	vips.LoggingSettings(
		vips_util.LoggingSettings,
		vips_util.ZapToVipsLoggingLevel(cfg.Logging.ZapLevel()),
	)
	vips.Startup(cfg.Vips.GoVips())
	defer vips.Shutdown()

	// PostgreSQL
	db, err := database.NewDB(cfg.Database)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Telemetry
	provider, err := telemetry.NewTraceProvider(
		ctx,
		cfg.Telemetry,
		semconv.ServiceNameKey.String(ApplicationName),
		semconv.ServiceVersionKey.String(build.Version+`-`+build.GitSHA),
	)
	if err != nil {
		log.Fatal(err)
	}
	defer func(provider *trace.TracerProvider, ctx context.Context) {
		err := provider.Shutdown(ctx)
		if err != nil {
			log.Errorw("failed to shutdown trace provider", zap.Error(err))
		}
	}(provider, context.Background())

	// NATSConnection

	natsConn, err := nats.NewConnection(*cfg.NATS)
	if err != nil {
		log.Fatal(err)
	}
	defer natsConn.Close()

	convertNatsPublisher := nats.NewPublisher[*convert.ConvertAndUploadInput](natsConn, convert.SubjectName)

	// S3
	minioOptions := &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.S3.AccessKey, cfg.S3.SecretKey, ""),
		Secure: cfg.S3.UseSSL,
		Region: cfg.S3.Region,
	}
	if cfg.Telemetry.Enable {
		minioTransport, err := minio.DefaultTransport(cfg.S3.UseSSL)
		if err != nil {
			log.Fatal(err)
		}
		minioOptions.Transport = otelhttp.NewTransport(
			minioTransport,
			otelhttp.WithMessageEvents(otelhttp.ReadEvents, otelhttp.WriteEvents),
		)
	}
	minioClient, err := minio.New(cfg.S3.Endpoint, minioOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Redis
	redisClient := cfg.Redis.Instance()
	redis.SetLogger(redis_util.NewLogger(func(_ context.Context, format string, v ...interface{}) {
		log.Named("redis").Sugar().Infof(format, v...)
	}))
	defer redisClient.Close()

	grpclog := logger_grpc.New(
		logger_grpc.WithLogger(log.Named("grpc")),
		logger_grpc.WithSkipper(func(service, method string, err error) bool {
			return service == grpc_health_v1.Health_ServiceDesc.ServiceName
		}),
	)

	// gRPC Transports
	transports, err := grpc_util.NewTransports(grpc_util.TransportBuilderConfig{
		Config:             cfg.GRPC,
		StreamInterceptors: []grpc.StreamServerInterceptor{grpclog.StreamServerInterceptor()},
		UnaryInterceptors:  []grpc.UnaryServerInterceptor{grpclog.UnaryServerInterceptor()},
		EnableTelemetry:    cfg.Telemetry.Enable,
	})
	if err != nil {
		log.Fatal(err)
	}

	// Services
	analyzerSvc := analyzer.NewService(db, minioClient, cfg.S3)
	uploadSvc := upload.NewService(
		cfg.URLPrefix,
		cfg.S3,
		db,
		analyzerSvc,
		minioClient,
		redisClient,
		convertNatsPublisher,
	)
	filterSvc := filter.NewService(db, cfg.URLPrefix)

	// Servers
	imageProcessorSrv := image_processor.NewGRPCService(db, uploadSvc, filterSvc)

	// gRPC Registration
	for _, transport := range transports {
		L := log.
			Named("grpc.transport").
			With(zap.String("addr", transport.Config.HostPort()))

		L.Debug(
			"registering grpc transport",
			zap.String("service", grpc_imageprocessor_v1.ImageProcessor_ServiceDesc.ServiceName),
		)
		grpc_imageprocessor_v1.RegisterImageProcessorServer(transport.Server, imageProcessorSrv)

		L.Debug("registering grpc reflection")
		reflection.Register(transport.Server)

		listener, err := transport.Listener()
		if err != nil {
			log.Fatal(err)
		}
		defer listener.Close()

		L.Info(
			"listening grpc transport",
			zap.Bool("secure", transport.Config.Credentials.Secure),
		)

		go func(server *grpc.Server) {
			if err := server.Serve(listener); err != nil {
				log.Fatal(err)
			}
		}(transport.Server)
	}

	<-ctx.Done()
}
