package enums

import (
	"fmt"
	"slices"

	"github.com/davidbyttow/govips/v2/vips"
	grpc_imageprocessor_common_v1 "gitlab.com/eemj/image-processor/api/grpc_imageprocessor_common/v1"
	mapsutil "gitlab.com/eemj/image-processor/pkg/maps_util"
)

type ImageFormat string

const (
	IMAGE_FORMAT_WEBP ImageFormat = "webp"
	IMAGE_FORMAT_JPEG ImageFormat = "jpeg"
	IMAGE_FORMAT_PNG  ImageFormat = "png"
	IMAGE_FORMAT_AVIF ImageFormat = "avif"
)

var (
	AllImageFormats = []ImageFormat{
		IMAGE_FORMAT_WEBP,
		IMAGE_FORMAT_JPEG,
		IMAGE_FORMAT_PNG,
		IMAGE_FORMAT_AVIF,
	}

	ExtImageFormats = map[ImageFormat]string{
		IMAGE_FORMAT_WEBP: "webp",
		IMAGE_FORMAT_JPEG: "jpg",
		IMAGE_FORMAT_PNG:  "png",
		IMAGE_FORMAT_AVIF: "avif",
	}

	ImageFormatExts = mapsutil.Swap(ExtImageFormats)

	MimeImageFormats = map[ImageFormat]string{
		IMAGE_FORMAT_WEBP: "image/webp",
		IMAGE_FORMAT_JPEG: "image/jpg",
		IMAGE_FORMAT_PNG:  "image/png",
		IMAGE_FORMAT_AVIF: "image/avif",
	}

	ApiImageFormats = map[grpc_imageprocessor_common_v1.ImageFormat]ImageFormat{
		grpc_imageprocessor_common_v1.ImageFormat_WEBP: IMAGE_FORMAT_WEBP,
		grpc_imageprocessor_common_v1.ImageFormat_JPEG: IMAGE_FORMAT_JPEG,
		grpc_imageprocessor_common_v1.ImageFormat_PNG:  IMAGE_FORMAT_PNG,
		grpc_imageprocessor_common_v1.ImageFormat_AVIF: IMAGE_FORMAT_AVIF,
	}

	ImageFormatsApi = mapsutil.Swap(ApiImageFormats)

	VipsImageFormats = map[vips.ImageType]ImageFormat{
		vips.ImageTypeWEBP: IMAGE_FORMAT_WEBP,
		vips.ImageTypeJPEG: IMAGE_FORMAT_JPEG,
		vips.ImageTypePNG:  IMAGE_FORMAT_PNG,
		vips.ImageTypeAVIF: IMAGE_FORMAT_AVIF,
	}

	ImageFormatsVips = mapsutil.Swap(VipsImageFormats)
)

func (f ImageFormat) String() string { return string(f) }

func (f ImageFormat) Enum() *ImageFormat { return &f }

func (f ImageFormat) Valid() bool { return slices.Contains(AllImageFormats, f) }

func (f ImageFormat) Ext() string { return ExtImageFormats[f] }

func (f ImageFormat) Mime() string { return MimeImageFormats[f] }

func (f ImageFormat) Vips() vips.ImageType { return ImageFormatsVips[f] }

func (f ImageFormat) Api() grpc_imageprocessor_common_v1.ImageFormat { return ImageFormatsApi[f] }

func (f *ImageFormat) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*f = ImageFormat(s)
	case string:
		*f = ImageFormat(s)
	default:
		return fmt.Errorf("unsupported scan type for ImageFormat: %T", src)
	}
	return nil
}

func ImageFormatFromExt(ext string) ImageFormat {
	return ImageFormatExts[ext]
}
