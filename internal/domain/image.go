package domain

import (
	"time"

	"github.com/google/uuid"
	"github.com/segmentio/ksuid"
	"gitlab.com/eemj/image-processor/internal/domain/bitflags"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
)

type Image struct {
	ID             uuid.UUID           `json:"id,omitempty"`
	ImageRequestID uuid.UUID           `json:"image_request_id,omitempty"`
	Reference      ksuid.KSUID         `json:"reference,omitempty"`
	ObjectKey      string              `json:"object_key,omitempty"`
	Size           int64               `json:"size,omitempty"`
	Height         int32               `json:"height,omitempty"`
	Width          int32               `json:"width,omitempty"`
	Format         enums.ImageFormat   `json:"format,omitempty"`
	SHA1           string              `json:"sha_1,omitempty"`
	Flags          bitflags.ImageFlags `json:"flags,omitempty"`
	CreatedAt      time.Time           `json:"created_at,omitempty"`
	UpdatedAt      time.Time           `json:"updated_at,omitempty"`
	DeletedAt      *time.Time          `json:"deleted_at,omitempty"`
}

type ImageRequest struct {
	ID            uuid.UUID   `json:"id,omitempty"`
	Reference     ksuid.KSUID `json:"reference,omitempty"`
	URL           string      `json:"url,omitempty"`
	SHA1          string      `json:"sha1,omitempty"`
	DominantColor *string     `json:"dominant_color,omitempty"`
	CreatedAt     time.Time   `json:"created_at,omitempty"`
	UpdatedAt     time.Time   `json:"updated_at,omitempty"`

	Images []*Image `json:"images,omitempty"`
}
