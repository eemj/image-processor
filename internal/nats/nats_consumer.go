package nats

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"

	"github.com/cenkalti/backoff/v4"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	"go.jamie.mt/logx/log"
	natsutil "go.jamie.mt/toolbox/nats_util"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type ConsumeHandler[T any] interface {
	Handle(ctx context.Context, msg T) (err error)
	Configurations() (jetstream.StreamConfig, jetstream.ConsumerConfig)
}

type Consumer[T any] interface {
	Consume(ctx context.Context) (err error)
}

type consumer[T any] struct {
	conn    *Connection
	logger  *zap.Logger
	tracer  trace.Tracer
	backoff backoff.BackOff
	handler ConsumeHandler[T]
}

func (c *consumer[T]) createStreamAndConsumer(ctx context.Context) (jetstream.Consumer, error) {
	js, err := jetstream.New(c.conn.conn)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize jetstream: %w", err)
	}

	streamConfig, consumerConfig := c.handler.Configurations()

	stream, err := js.Stream(ctx, streamConfig.Name)
	if err != nil {
		if !errors.Is(err, jetstream.ErrStreamNotFound) {
			return nil, fmt.Errorf("failed to query stream: %w", err)
		}
		stream, err = js.CreateStream(ctx, streamConfig)
		if err != nil {
			return nil, fmt.Errorf("failed to create stream: %w", err)
		}
	}

	consumer, err := stream.Consumer(ctx, consumerConfig.Name)
	if err != nil {
		if !errors.Is(err, jetstream.ErrConsumerNotFound) {
			return nil, fmt.Errorf("failed to query consumer: %w", err)
		}
		consumer, err = stream.CreateConsumer(ctx, consumerConfig)
		if err != nil {
			return nil, fmt.Errorf("failed to create consumer: %w", err)
		}
	}

	return consumer, nil
}

// Consume implements Consumer.
func (c *consumer[T]) Consume(ctx context.Context) (err error) {
	for ctx.Err() == nil {
		backoffDuration := c.backoff.NextBackOff()
		if backoffDuration == backoff.Stop {
			return fmt.Errorf("backoff duration signalled to stop")
		}

		consumer, err := c.createStreamAndConsumer(ctx)
		if err != nil {
			c.logger.Error("failed to create or update consumer", zap.Error(err))
			continue
		}

		for ctx.Err() == nil {
			messageBatch, err := consumer.FetchNoWait(1)
			if err != nil {
				c.logger.Error(
					"failed to fetch from consumer",
					zap.Error(err),
				)
				break
			}
			for message := range messageBatch.Messages() {
				natsMsg := &nats.Msg{
					Subject: message.Subject(),
					Reply:   message.Reply(),
					Header:  message.Headers(),
					Data:    message.Data(),
				}
				spanContext, _ := natsutil.GetTraceContextFromMsg(natsMsg)
				messageContext := trace.ContextWithSpanContext(ctx, spanContext)
				decodedMsg := new(T)
				err = json.Unmarshal(natsMsg.Data, decodedMsg)
				if err != nil {
					c.logger.Error(
						"failed to decode message",
						zap.ByteString("data", natsMsg.Data),
						zap.Error(err),
					)
					continue
				}

				err = message.InProgress()
				if err != nil {
					c.logger.Error(
						"failed to in-progress acknowledge message",
						zap.ByteString("data", natsMsg.Data),
						zap.Error(err),
					)
					continue
				}

				err = c.handler.Handle(messageContext, *decodedMsg)
				if err != nil {
					c.logger.Error(
						"failed to handle message",
						zap.ByteString("data", natsMsg.Data),
						zap.Error(err),
					)
					// Intentionally don't do anything here
					// If the handle returns an error - logging it is enough
					// Acknowledge the message - regardless.
				}

				err = message.Ack()
				if err != nil {
					c.logger.Error(
						"failed to acknowledge message",
						zap.ByteString("data", natsMsg.Data),
						zap.Error(err),
					)
					continue
				}
			}
			err = messageBatch.Error()
			if err != nil {
				return err
			}
		}

		err = ctx.Err()
		if err != nil {
			return err
		}
	}
	return ctx.Err()
}

func NewConsumer[T any](
	conn *Connection,
	handler ConsumeHandler[T],
) (Consumer[T], error) {
	return &consumer[T]{
		logger:  log.Named(common.NATSConsumerName),
		tracer:  otel.Tracer(reflect.TypeFor[consumer[T]]().PkgPath()),
		backoff: backoff.NewExponentialBackOff(),
		conn:    conn,
		handler: handler,
	}, nil
}
