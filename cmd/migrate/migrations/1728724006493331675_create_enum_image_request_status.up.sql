do
$$
    begin
        if not exists (select 1 from pg_type where typname = 'image_request_status') then
            create type image_request_status as enum ('new', 'queued', 'ongoing', 'incomplete', 'complete');
        end if;
    end
$$;

alter table image_request
    add column if not exists status image_request_status not null default 'new'::image_request_status;
