package analyzer

import (
	"context"
	"math/rand"
	"net/http"
	"reflect"
	"time"

	"github.com/minio/minio-go/v7"
	"gitlab.com/eemj/image-processor/internal/database"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	useragents "gitlab.com/eemj/image-processor/pkg/user_agents"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/configmodels"
	httptransports "go.jamie.mt/toolbox/http_util/transports"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type service struct {
	tracer     trace.Tracer
	logger     *zap.Logger
	db         database.DB
	s3         *minio.Client
	s3Config   configmodels.S3
	httpClient *http.Client
}

func (s *service) L(ctx context.Context) *zap.Logger {
	return log.InjectActivity(ctx, s.logger)
}

type Service interface {
	Prepare(ctx context.Context, param AnalyzerPrepareParams) (*AnalyzerPrepareResult, error)
	Pending(ctx context.Context, param AnalyzerPendingParams) (*AnalyzerPendingResult, error)
}

func NewService(
	db database.DB,
	s3 *minio.Client,
	s3Config configmodels.S3,
) Service {
	return &service{
		tracer:   otel.Tracer(reflect.TypeFor[Service]().PkgPath()),
		logger:   log.Named(common.AnalyzerServiceName),
		db:       db,
		s3:       s3,
		s3Config: s3Config,
		httpClient: &http.Client{Transport: &httptransports.UserAgentTransport{
			Generator: func() string {
				return useragents.UserAgents[rand.Intn(len(useragents.UserAgents))]
			},
			Transport: &httptransports.RetryTransport{
				Transport:     http.DefaultTransport,
				RetryCount:    3,
				RetryDuration: (250 * time.Millisecond),
			},
		}},
	}
}
