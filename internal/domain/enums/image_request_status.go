package enums

import (
	"fmt"
	"slices"
)

type ImageRequestStatus string

const (
	IMAGE_REQUEST_STATUS_NEW        ImageRequestStatus = "new"
	IMAGE_REQUEST_STATUS_QUEUED     ImageRequestStatus = "queued"
	IMAGE_REQUEST_STATUS_ONGOING    ImageRequestStatus = "ongoing"
	IMAGE_REQUEST_STATUS_INCOMPLETE ImageRequestStatus = "incomplete"
	IMAGE_REQUEST_STATUS_COMPLETE   ImageRequestStatus = "complete"
)

var AllImageRequestStatuses = []ImageRequestStatus{
	IMAGE_REQUEST_STATUS_NEW,
	IMAGE_REQUEST_STATUS_QUEUED,
	IMAGE_REQUEST_STATUS_ONGOING,
	IMAGE_REQUEST_STATUS_INCOMPLETE,
	IMAGE_REQUEST_STATUS_COMPLETE,
}

func (s ImageRequestStatus) String() string { return string(s) }

func (s ImageRequestStatus) Enum() *ImageRequestStatus { return &s }

func (s ImageRequestStatus) Valid() bool { return slices.Contains(AllImageRequestStatuses, s) }

func (f *ImageRequestStatus) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*f = ImageRequestStatus(s)
	case string:
		*f = ImageRequestStatus(s)
	default:
		return fmt.Errorf("unsupported scan type for ImageRequestStatus: %T", src)
	}
	return nil
}
