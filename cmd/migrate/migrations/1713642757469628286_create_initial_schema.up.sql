create extension if not exists "uuid-ossp";

create type image_format as enum ('webp', 'jpeg', 'png', 'avif');

create table if not exists image_request
(
    id          uuid                        not null default uuid_generate_v4(),
    reference   char(27)                    not null,
    url         text                        not null,
    sha1        char(40)                    not null,
    formats     image_format[]              not null default '{}',
    parameters  jsonb                       not null default '{}',
    created_at  timestamp without time zone not null default (current_timestamp at time zone 'utc'),
    updated_at  timestamp without time zone not null default (current_timestamp at time zone 'utc')
);

alter table image_request
    add constraint image_request_pk primary key (id),
    add constraint image_request_id_sha1_uq unique (sha1);

create table if not exists image
(
    id               uuid                        not null default uuid_generate_v4(),
    image_request_id uuid                        not null,
    reference        char(27)                    not null,
    object_key       text                        not null,
    size             bigint                      not null,
    height           int                         not null,
    width            int                         not null,
    format           image_format                not null,
    sha1             char(40)                    not null,
    flags            int                         not null default 0,
    dominant_color   char(6),
    created_at       timestamp without time zone not null default (current_timestamp at time zone 'utc'),
    updated_at       timestamp without time zone not null default (current_timestamp at time zone 'utc'),
    deleted_at       timestamp without time zone
);

alter table image
    add constraint image_pk primary key (id),
    add constraint image_image_request_id_fk foreign key (image_request_id) references image_request (id),
    add constraint image_reference_format unique (reference, format);

create index if not exists image_sha1_ix on image (sha1);
create index if not exists image_image_request_id_ix on image (image_request_id);
