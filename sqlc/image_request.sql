-- name: InsertImageRequest :one
insert into image_request
(
    reference,
    url,
    sha1,
    status,
    dominant_color
) values (
    sqlc.arg('reference'),
    sqlc.arg('url'),
    sqlc.arg('sha1'),
    sqlc.arg('status')::image_request_status,
    sqlc.narg('dominant_color')
)
returning id;

-- name: UpdateImageRequest :exec
update image_request
set
    reference = coalesce(sqlc.narg('reference'), reference),
    dominant_color = (case when sqlc.narg('dominant_color')::text is not null then sqlc.narg('dominant_color')::text else dominant_color end),
    status = (case when sqlc.narg('status')::image_request_status is null then status else sqlc.narg('status')::image_request_status end),
    updated_at = (current_timestamp at time zone 'utc')
where id = sqlc.arg('id');

-- name: FirstImageRequest :one
select * from image_request where id = sqlc.arg('id');

-- name: FindImageRequest :many
select
    image_request.*,
    sqlc.embed(image)
from image_request
join image on image.image_request_id = image_request.id
where 
    (array_length(sqlc.narg('urls')::text[], 1) is null or image_request.url = any(sqlc.narg('urls')::text[])) and
    (array_length(sqlc.narg('references')::text[], 1) is null or image_request.reference = any(sqlc.narg('references')::text[]));
