package common

import (
	"strings"

	"github.com/segmentio/ksuid"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
)

// CreateObjectKey builds the object key with a reference and the image format.
// Returning the image format of: `Reference`/`Reference`.`Extension`.
func CreateObjectKey(reference ksuid.KSUID, format enums.ImageFormat) string {
	var sb strings.Builder
	sb.WriteString(reference.String())
	sb.WriteRune('/')
	sb.WriteString(reference.String())
	sb.WriteRune('.')
	sb.WriteString(format.Ext())
	return sb.String()
}
