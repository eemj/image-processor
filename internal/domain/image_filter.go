package domain

import (
	"time"

	"github.com/segmentio/ksuid"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
)

type ImageFilterItem struct {
	Reference     ksuid.KSUID      `json:"reference,omitempty"`
	OriginURL     string           `json:"origin_url,omitempty"`
	PublicURL     string           `json:"public_url,omitempty"`
	Dimensions    *ImageDimensions `json:"dimensions,omitempty"`
	DominantColor *string          `json:"dominant_color,omitempty"`
	Metadata      []*ImageMetadata `json:"metadata,omitempty"`
	CreatedAt     time.Time        `json:"created_at,omitempty"`
	UpdatedAt     time.Time        `json:"updated_at,omitempty"`
}

type ImageChecksums struct {
	SHA1 string `json:"sha_1,omitempty"`
}

type ImageDimensions struct {
	Height int64 `json:"height,omitempty"`
	Width  int64 `json:"width,omitempty"`
}

type ImageMetadata struct {
	Format    enums.ImageFormat `json:"format,omitempty"`
	Checksums ImageChecksums    `json:"checksums,omitempty"`
	Size      int64             `json:"size,omitempty"`
}
