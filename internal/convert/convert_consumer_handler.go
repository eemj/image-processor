package convert

import (
	"context"
	"errors"
	"reflect"

	"github.com/google/uuid"
	"github.com/minio/minio-go/v7"
	"gitlab.com/eemj/image-processor/internal/analyzer"
	"gitlab.com/eemj/image-processor/internal/database"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/common"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
	"gitlab.com/eemj/image-processor/internal/nats"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/configmodels"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type ConvertAndUploadInput struct {
	ImageRequestID  uuid.UUID                     `json:"image_request_id,omitempty"`
	ImageFormats    []enums.ImageFormat           `json:"image_formats,omitempty"`
	ImageParameters domain.ImageRequestParameters `json:"image_parameters,omitempty"`
}

type consumerHandler struct {
	db     database.DB
	logger *zap.Logger
	trace  trace.Tracer

	minioClient *minio.Client
	s3Config    configmodels.S3
	analyzerSvc analyzer.Service
}

// Handle implements nats.ConsumeHandler.
func (c *consumerHandler) Handle(ctx context.Context, input *ConvertAndUploadInput) (err error) {
	err = c.db.Q().UpdateImageRequest(ctx, sqlc.UpdateImageRequestParams{
		ID:     input.ImageRequestID,
		Status: enums.IMAGE_REQUEST_STATUS_ONGOING.Enum(),
	})
	if err != nil {
		c.logger.Error(
			"failed to update image request status",
			zap.Error(err),
			zap.Stringer("status", enums.IMAGE_REQUEST_STATUS_ONGOING),
			zap.Stringer("image_request_id", input.ImageRequestID),
		)
		return err
	}

	if input.ImageParameters.ComputeDominantColor {
		c.logger.Info(
			"processing dominant color",
			zap.Stringp("fallback_dominant_colour", input.ImageParameters.FallbackDominantColor),
		)

		processDominantColorErr := c.processDominantColour(ctx, &ProcessDominantColorInput{
			ImageRequestID:        input.ImageRequestID,
			FallbackDominantColor: input.ImageParameters.FallbackDominantColor,
		})
		if processDominantColorErr != nil {
			err = c.db.Q().UpdateImageRequest(ctx, sqlc.UpdateImageRequestParams{
				ID:     input.ImageRequestID,
				Status: enums.IMAGE_REQUEST_STATUS_INCOMPLETE.Enum(),
			})
			if err != nil {
				c.logger.Error(
					"failed to update image request status",
					zap.Error(err),
					zap.Stringer("status", enums.IMAGE_REQUEST_STATUS_INCOMPLETE),
					zap.Stringer("image_request_id", input.ImageRequestID),
				)
				return err
			}
			return processDominantColorErr
		}
	}

	convertErrors := make([]error, 0, len(input.ImageFormats))
	for _, format := range input.ImageFormats {
		convertImageReply, err := c.convertImage(ctx, &ConvertImageInput{
			ImageRequestID: input.ImageRequestID,
			ImageFormat:    format,
		})
		if err != nil {
			c.logger.Error("failed whilst converting an image", zap.Error(err))

			convertErrors = append(convertErrors, err)
		} else {
			c.logger.Info(
				"successfully converted image",
				zap.Stringer("format", convertImageReply.ImageFormat),
				zap.Stringer("image_id", convertImageReply.ImageID),
				zap.String("object_key", convertImageReply.ObjectKey),
			)
		}
	}

	var (
		status     *enums.ImageRequestStatus
		convertErr = errors.Join(convertErrors...)
	)
	if convertErr != nil {
		status = enums.IMAGE_REQUEST_STATUS_INCOMPLETE.Enum()
	} else {
		status = enums.IMAGE_REQUEST_STATUS_COMPLETE.Enum()
	}

	err = c.db.Q().UpdateImageRequest(ctx, sqlc.UpdateImageRequestParams{
		ID:     input.ImageRequestID,
		Status: status,
	})
	if err != nil {
		c.logger.Error(
			"failed to update image request status",
			zap.Error(err),
			zap.Stringer("status", status),
			zap.Stringer("image_request_id", input.ImageRequestID),
		)
		return err
	}

	return convertErr
}

var _ nats.ConsumeHandler[*ConvertAndUploadInput] = &consumerHandler{}

func (c *consumerHandler) L(ctx context.Context) *zap.Logger {
	return log.InjectActivity(ctx, c.logger)
}

func NewConsumerHandler(
	db database.DB,
	minioClient *minio.Client,
	s3Config configmodels.S3,
	analyzerSvc analyzer.Service,
) nats.ConsumeHandler[*ConvertAndUploadInput] {
	return &consumerHandler{
		db:          db,
		logger:      log.Named(common.ConvertConsumerHandlerName),
		trace:       otel.Tracer(reflect.TypeFor[consumerHandler]().PkgPath()),
		minioClient: minioClient,
		s3Config:    s3Config,
		analyzerSvc: analyzerSvc,
	}
}
