package analyzer

import (
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain"
)

func transformFindImagesWithImageRequestID(rows ...*sqlc.FindImagesWithImageRequestIDRow) *domain.ImageRequest {
	if len(rows) == 0 {
		return nil
	}
	imageRequest := &domain.ImageRequest{
		Images: make([]*domain.Image, len(rows)),
	}
	if len(rows) > 0 {
		imageRequest.ID = rows[0].ID
		imageRequest.Reference = rows[0].Reference
		imageRequest.URL = rows[0].URL
		imageRequest.SHA1 = rows[0].SHA1
		imageRequest.CreatedAt = rows[0].CreatedAt
		imageRequest.UpdatedAt = rows[0].UpdatedAt
	}
	for index := range rows {
		imageRequest.Images[index] = (*domain.Image)(&rows[index].Image)
	}
	return imageRequest
}

func transformFindImagesWithSHA1(rows ...*sqlc.FindImagesWithSha1Row) *domain.ImageRequest {
	if len(rows) == 0 {
		return nil
	}
	imageRequest := &domain.ImageRequest{
		Images: make([]*domain.Image, len(rows)),
	}
	if len(rows) > 0 {
		imageRequest.ID = rows[0].ID
		imageRequest.Reference = rows[0].Reference
		imageRequest.URL = rows[0].URL
		imageRequest.SHA1 = rows[0].SHA1
		imageRequest.CreatedAt = rows[0].CreatedAt
		imageRequest.UpdatedAt = rows[0].UpdatedAt
	}
	for index := range rows {
		imageRequest.Images[index] = (*domain.Image)(&rows[index].Image)
	}
	return imageRequest
}
