package vips

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/davidbyttow/govips/v2/vips"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
)

type Collector struct {
	memoryBytesMetric    *prometheus.Desc
	memoryMaxBytesMetric *prometheus.Desc
	allocsMetric         *prometheus.Desc
	filesOpenMetric      *prometheus.Desc

	runtimeMetricLock sync.RWMutex
	runtimeMetrics    map[string]*prometheus.Desc

	conversion *prometheus.HistogramVec
}

// Collect implements prometheus.Collector.
func (c *Collector) Collect(ch chan<- prometheus.Metric) {
	var (
		runtimeStats vips.RuntimeStats
		memoryStats  vips.MemoryStats
	)
	vips.ReadRuntimeStats(&runtimeStats)
	vips.ReadVipsMemStats(&memoryStats)

	ch <- prometheus.MustNewConstMetric(c.memoryBytesMetric, prometheus.GaugeValue, float64(memoryStats.Mem))
	ch <- prometheus.MustNewConstMetric(c.memoryMaxBytesMetric, prometheus.GaugeValue, float64(memoryStats.MemHigh))
	ch <- prometheus.MustNewConstMetric(c.allocsMetric, prometheus.GaugeValue, float64(memoryStats.Allocs))
	ch <- prometheus.MustNewConstMetric(c.filesOpenMetric, prometheus.GaugeValue, float64(memoryStats.Files))

	c.runtimeMetricLock.Lock()
	defer c.runtimeMetricLock.Unlock()
	for operation, count := range runtimeStats.OperationCounts {
		runtimeDesc, exists := c.runtimeMetrics[operation]
		if !exists {
			runtimeDesc = prometheus.NewDesc(
				(`vips_operation_` + strings.ToLower(operation) + `_count`),
				fmt.Sprintf("Vips operation counter for the operation '%s'", operation),
				nil, nil,
			)
			c.runtimeMetrics[operation] = runtimeDesc
		}

		ch <- prometheus.MustNewConstMetric(runtimeDesc, prometheus.GaugeValue, float64(count))
	}
}

// Describe implements prometheus.Collector.
func (c *Collector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.memoryBytesMetric
	ch <- c.memoryMaxBytesMetric
	ch <- c.allocsMetric
	ch <- c.filesOpenMetric

	if c.runtimeMetrics != nil {
		c.runtimeMetricLock.RLock()
		defer c.runtimeMetricLock.RUnlock()
		for _, runtimeMetricDesc := range c.runtimeMetrics {
			ch <- runtimeMetricDesc
		}
	}
}

var _ prometheus.Collector = &Collector{}

func (c *Collector) Conversion(from, to enums.ImageFormat, duration time.Duration) {
	c.conversion.
		With(prometheus.Labels{
			"from": from.String(),
			"to":   to.String(),
		}).
		Observe(float64(duration.Milliseconds()))
}

func NewCollector() *Collector {
	return &Collector{
		memoryBytesMetric:    prometheus.NewDesc(`vips_memory_bytes`, "A gauge of the vips tracked memory usage in bytes", nil, nil),
		memoryMaxBytesMetric: prometheus.NewDesc(`vips_memory_max_bytes`, `A gauge of the max vips tracked memory usage in bytes`, nil, nil),
		allocsMetric:         prometheus.NewDesc(`vips_allocs`, `A gauge of the number of active vips allocations`, nil, nil),
		filesOpenMetric:      prometheus.NewDesc(`vips_open_files`, `A gauge of the currently open files opened by vips`, nil, nil),
		conversion: promauto.NewHistogramVec(prometheus.HistogramOpts{
			Namespace: "vips",
			Subsystem: "conversion",
			Name:      "duration_milliseconds",
			Help:      "A histogram that holds the duration of a conversion between image types.",
			Buckets:   []float64{500, 4_000, 7_500, 11_000, 14_500, 18_000, 21_500, 25_000, 28_500, 31_000},
		}, []string{"from", "to"}),

		runtimeMetrics: make(map[string]*prometheus.Desc),
	}
}
