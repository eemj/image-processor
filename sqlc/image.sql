-- name: InsertImage :one
insert into image
(
    image_request_id,
    reference,
    object_key,
    size,
    height,
    width,
    format,
    sha1,
    flags
) values (
    sqlc.arg('image_request_id'),
    sqlc.arg('reference'),
    sqlc.arg('object_key'),
    sqlc.arg('size'),
    sqlc.arg('height'),
    sqlc.arg('width'),
    sqlc.arg('format'),
    sqlc.arg('sha1'),
    sqlc.arg('flags')
)
returning id;

-- name: FindImagesWithSha1 :many
select
    image_request.*,
    sqlc.embed(image)
from image_request
left join image on image_request.id = image.image_request_id
where
    image_request.sha1 = sqlc.arg('sha1') or
    image_request.id in (
        select image.image_request_id
        from image
        where image.sha1 = sqlc.arg('sha1')
    );

-- name: FindImagesWithImageRequestID :many
select
    image_request.*,
    sqlc.embed(image)
from image_request
left join image on image_request.id = image.image_request_id
where image_request.id = sqlc.arg('image_request_id');

-- name: UpdateImage :exec
update image
set
    flags = (case when sqlc.narg('flags')::int is not null then sqlc.narg('flags')::int else flags end),
    updated_at = (current_timestamp at time zone 'utc')
where id = sqlc.arg('id');
