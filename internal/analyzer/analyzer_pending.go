package analyzer

import (
	"context"
	"fmt"
	"path"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/minio/minio-go/v7"
	"gitlab.com/eemj/image-processor/internal/database/sqlc"
	"gitlab.com/eemj/image-processor/internal/domain"
	"gitlab.com/eemj/image-processor/internal/domain/enums"
)

type AnalyzerPendingParams struct {
	ImageRequestID  *uuid.UUID                     `json:"image_request_id,omitempty"`
	ImageRequest    *domain.ImageRequest           `json:"image_request,omitempty"`
	ImageFormats    []enums.ImageFormat            `json:"image_formats,omitempty"`
	ImageParameters *domain.ImageRequestParameters `json:"image_parameters,omitempty"`
}

type AnalyzerPendingImageFormat struct {
	Format enums.ImageFormat `json:"format,omitempty"`

	S3       bool `json:"s3,omitempty"`
	Database bool `json:"database,omitempty"`

	Image *domain.Image `json:"image,omitempty"`
}

type AnalyzerPendingResult struct {
	ImageRequest *domain.ImageRequest `json:"image_request,omitempty"`

	DominantColor bool `json:"dominant_color,omitempty"`
	S3            bool `json:"s3,omitempty"`
	Database      bool `json:"database,omitempty"`

	Formats []*AnalyzerPendingImageFormat `json:"items,omitempty"`
}

func (s *service) Pending(ctx context.Context, param AnalyzerPendingParams) (*AnalyzerPendingResult, error) {
	var (
		imageRequest    = param.ImageRequest
		imageFormats    = param.ImageFormats
		imageParameters = param.ImageParameters
	)

	// `param.ImageRequest` wasn't provided
	if imageRequest == nil && param.ImageRequestID != nil {
		rows, err := s.db.Q().FindImagesWithImageRequestID(ctx, sqlc.FindImagesWithImageRequestIDParams{
			ImageRequestID: *param.ImageRequestID,
		})
		if err != nil && err != pgx.ErrNoRows {
			return nil, err
		}
		imageRequest = transformFindImagesWithImageRequestID(rows...)
	}
	if imageRequest == nil {
		return nil, fmt.Errorf("`image_request` not found with id `%s`", param.ImageRequestID)
	}

	// `param.ImageFormats` wasn't provided
	if len(imageFormats) == 0 {
		imageFormats = make([]enums.ImageFormat, len(imageRequest.Images))
		for index, image := range imageRequest.Images {
			imageFormats[index] = image.Format
		}
	}

	imageFormatsLength := len(imageFormats)

	pendingFormatsMap := make(map[enums.ImageFormat]*AnalyzerPendingImageFormat, imageFormatsLength)

	for _, imageFormat := range imageFormats {
		pendingImage := &AnalyzerPendingImageFormat{Format: imageFormat}

		for _, image := range imageRequest.Images {
			if image.Format == imageFormat {
				pendingImage.Image = image
				pendingImage.Database = true
				break
			}
		}

		pendingFormatsMap[imageFormat] = pendingImage
	}

	for object := range s.s3.ListObjects(ctx, s.s3Config.Bucket, minio.ListObjectsOptions{
		Prefix: (imageRequest.Reference.String() + "/"),
	}) {
		err := object.Err
		if err != nil {
			return nil, err
		}

		ext := strings.TrimPrefix(path.Ext(object.Key), ".")
		imageFormat := enums.ImageFormatFromExt(ext)

		_, exists := pendingFormatsMap[imageFormat]
		if exists {
			pendingFormatsMap[imageFormat].S3 = true
		}
	}

	var (
		pendingDatabase int
		pendingS3       int
		pendingResult   = &AnalyzerPendingResult{
			ImageRequest:  imageRequest,
			Formats:       make([]*AnalyzerPendingImageFormat, 0, len(pendingFormatsMap)),
			DominantColor: ((imageParameters != nil && !imageParameters.ComputeDominantColor) || imageRequest.DominantColor != nil),
		}
	)
	for _, pendingFormat := range pendingFormatsMap {
		if pendingFormat.Database {
			pendingDatabase++
		}
		if pendingFormat.S3 {
			pendingS3++
		}
		pendingResult.Formats = append(pendingResult.Formats, pendingFormat)
	}

	return pendingResult, nil
}
